# IDDE Lab Assignments

- How to get the spring api running:
  - `docker-compose -f docker-compose-spring.yml up`  
  
- How to get the old app running:
    - `docker-compose up --build`
    - if using `prod` profile make sure to have `createTables: true` in `application-prod.json` on the first run
    <br/>
- Available profiles:
    - `prod`: postgres, jdbc, hikari
    - `dev`: in-memory  
    <br/>  
- Switch profiles by changing `JAVA_OPTS` in `docker-compose.yml`
