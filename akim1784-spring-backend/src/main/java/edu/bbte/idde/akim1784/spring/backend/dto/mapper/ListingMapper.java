package edu.bbte.idde.akim1784.spring.backend.dto.mapper;

import edu.bbte.idde.akim1784.spring.backend.dto.incoming.ListingCreationDto;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.ListingUpdateDto;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.ListingDto;
import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import edu.bbte.idde.akim1784.spring.backend.service.OwnerService;
import edu.bbte.idde.akim1784.spring.backend.service.PropertyService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ListingMapper {
    private final ModelMapper mapper;
    private final OwnerService ownerService;
    private final PropertyService propertyService;

    public ListingMapper(ModelMapper mapper, OwnerService ownerService, PropertyService propertyService) {
        this.mapper = mapper;
        this.ownerService = ownerService;
        this.propertyService = propertyService;
    }

    public Listing incomingDtoToModel(ListingCreationDto listingCreationDto) {
        Listing listing = mapper.map(listingCreationDto, Listing.class);
        listing.setOwner(ownerService.findById(listingCreationDto.getOwnerId()));
        listing.setProperty(propertyService.findById(listingCreationDto.getPropertyId()));
        return listing;
    }

    public ListingDto modelToDto(Listing listing) {
        return mapper.map(listing, ListingDto.class);
    }

    public void updateFromDto(Listing listing, ListingUpdateDto listingUpdateDto) {
        mapper.map(listingUpdateDto, listing);
        if (listingUpdateDto.getOwnerId() != null) {
            listing.setOwner(ownerService.findById(listingUpdateDto.getOwnerId()));
        }
        if (listingUpdateDto.getPropertyId() != null) {
            listing.setProperty(propertyService.findById(listingUpdateDto.getPropertyId()));
        }
    }
}
