package edu.bbte.idde.akim1784.spring.backend.util;

public class NumberConverter {
    public static Double stringToDoubleOrNull(String string) {
        return string == null ? null : Double.parseDouble(string);
    }
}
