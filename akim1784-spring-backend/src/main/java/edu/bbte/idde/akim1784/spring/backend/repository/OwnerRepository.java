package edu.bbte.idde.akim1784.spring.backend.repository;

import edu.bbte.idde.akim1784.spring.backend.model.Owner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {
    Page<Owner> findAllByNameContainsIgnoreCase(String name, Pageable pageable);
}
