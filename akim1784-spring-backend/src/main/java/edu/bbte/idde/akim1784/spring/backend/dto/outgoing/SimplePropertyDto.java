package edu.bbte.idde.akim1784.spring.backend.dto.outgoing;

import edu.bbte.idde.akim1784.spring.backend.model.PropertyType;

public class SimplePropertyDto {

    private Long id;
    private PropertyType type;
    private Double size;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }
}
