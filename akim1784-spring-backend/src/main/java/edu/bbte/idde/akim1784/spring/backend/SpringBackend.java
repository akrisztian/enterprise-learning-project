package edu.bbte.idde.akim1784.spring.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SpringBackend {
    public static void main(String[] args) {
        SpringApplication.run(SpringBackend.class, args);
    }
}
