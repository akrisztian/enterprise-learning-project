package edu.bbte.idde.akim1784.spring.backend.service.impl;

import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import edu.bbte.idde.akim1784.spring.backend.repository.ListingRepository;
import edu.bbte.idde.akim1784.spring.backend.service.ListingService;
import edu.bbte.idde.akim1784.spring.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ListingServiceImpl implements ListingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListingService.class);
    private final ListingRepository listingRepository;

    public ListingServiceImpl(ListingRepository listingRepository) {
        this.listingRepository = listingRepository;
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "listings", allEntries = true),
                    @CacheEvict(value = "listings-price", allEntries = true),
                    @CacheEvict(value = "owner-listings", allEntries = true),
                    @CacheEvict(value = "property-listings", allEntries = true)
            },
            put = @CachePut(value = "listing-single", key = "#entity.id")
    )
    @Override
    public Listing save(Listing entity) {
        LOGGER.info("Save listing: {}", entity);
        return listingRepository.save(entity);
    }

    @Cacheable(value = "listing-single", key = "#id")
    @Override
    public Listing findById(Long id) {
        LOGGER.info("Find listing with ID: {}", id);
        return listingRepository.findById(id).orElseThrow(
                () -> new ServiceException("Could not select listing with ID: " + id)
        );
    }

    @Cacheable(value = "listings", key = "{#page, #size}")
    @Override
    public Page<Listing> findAll(int page, int size) {
        LOGGER.info("Find {} listings on page: {}", size, page);
        return listingRepository.findAll(PageRequest.of(page, size));
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "listings", allEntries = true),
                    @CacheEvict(value = "listings-price", allEntries = true),
                    @CacheEvict(value = "listing-single", key = "#entity.id"),
                    @CacheEvict(value = "owner-listings", allEntries = true),
                    @CacheEvict(value = "property-listings", allEntries = true)
            }
    )
    @Override
    public void delete(Listing entity) {
        LOGGER.info("Delete listing: {}", entity);
        listingRepository.delete(entity);
    }

    @Cacheable(value = "listings-price", key = "{#minPrice, #maxPrice, #page, #size}")
    @Override
    public Page<Listing> findAllByPriceBetween(Double minPrice, Double maxPrice, int page, int size) {
        LOGGER.info("Find {} listing with price between: {} and {} on page {}", size, minPrice, maxPrice, page);
        PageRequest pageRequest = PageRequest.of(page, size);
        if (minPrice == null && maxPrice == null) {
            return listingRepository.findAll(pageRequest);
        }

        if (minPrice == null) {
            return listingRepository.findAllByPriceLessThanEqual(maxPrice, pageRequest);
        }

        if (maxPrice == null) {
            return listingRepository.findAllByPriceGreaterThanEqual(minPrice, pageRequest);
        }

        return listingRepository.findAllByPriceBetween(minPrice, maxPrice, pageRequest);
    }

    @Cacheable(value = "owner-listings", key = "{#id, #page, #size}")
    @Override
    public Page<Listing> findAllByOwnerId(Long id, int page, int size) {
        LOGGER.info("Find {} listing with owner ID: {} on page {}", size, id, page);
        return listingRepository.findAllByOwnerId(id, PageRequest.of(page, size));
    }

    @Cacheable(value = "property-listings", key = "{#id, #page, #size}")
    @Override
    public Page<Listing> findAllByPropertyId(Long id, int page, int size) {
        LOGGER.info("Find {} listings with property ID: {} on page {}", size, id, page);
        return listingRepository.findAllByPropertyId(id, PageRequest.of(page, size));
    }
}
