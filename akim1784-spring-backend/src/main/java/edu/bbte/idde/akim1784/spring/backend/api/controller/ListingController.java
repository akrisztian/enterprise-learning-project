package edu.bbte.idde.akim1784.spring.backend.api.controller;

import edu.bbte.idde.akim1784.spring.backend.api.ApiException;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.ListingCreationDto;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.ListingUpdateDto;
import edu.bbte.idde.akim1784.spring.backend.dto.mapper.ListingMapper;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.ListingDto;
import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import edu.bbte.idde.akim1784.spring.backend.service.ListingService;
import edu.bbte.idde.akim1784.spring.backend.service.ServiceException;
import edu.bbte.idde.akim1784.spring.backend.util.NumberConverter;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/listings")
public class ListingController {

    private final ListingService listingService;
    private final ListingMapper listingMapper;

    public ListingController(ListingService listingService, ListingMapper listingMapper) {
        this.listingService = listingService;
        this.listingMapper = listingMapper;
    }

    @GetMapping(params = {"!minPrice", "!maxPrice"})
    public ResponseEntity<List<ListingDto>> getAllListingsOnPage(@RequestParam(required = false) Integer page,
                                                                 @RequestParam(required = false) Integer size) {
        try {
            Page<Listing> listingPage = listingService.findAll(
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(listingPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    listingPage.stream().map(listingMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get all Listings", e);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @GetMapping
    public ResponseEntity<List<ListingDto>> getAllListingsByPriceBetween(
            @RequestParam Map<String, String> prices,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        try {
            Page<Listing> listingPage = listingService.findAllByPriceBetween(
                    NumberConverter.stringToDoubleOrNull(prices.get("minPrice")),
                    NumberConverter.stringToDoubleOrNull(prices.get("maxPrice")),
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(listingPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    listingPage.stream().map(listingMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get all Listings", e);
        } catch (NumberFormatException e) {
            throw new ApiException("Prices should be valid real numbers", e);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ListingDto> getListingById(@PathVariable(name = "id") Long id) {
        try {
            return ResponseEntity.ok(listingMapper.modelToDto(listingService.findById(id)));
        } catch (ServiceException e) {
            throw new ApiException("Could not get Listing with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<ListingDto> createListing(@RequestBody @Valid ListingCreationDto listingCreationDto) {
        try {
            Listing listing = listingService.save(listingMapper.incomingDtoToModel(listingCreationDto));
            return ResponseEntity
                    .created(URI.create("listings/" + listing.getId()))
                    .body(listingMapper.modelToDto(listing));
        } catch (ServiceException e) {
            throw new ApiException("Could not save Listing", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteListing(@PathVariable(name = "id") Long id) {
        try {
            listingService.delete(listingService.findById(id));
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            throw new ApiException("No listing with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ListingDto> updateListing(@PathVariable(name = "id") Long id,
                                                    @RequestBody @Valid ListingUpdateDto listingUpdateDto) {
        try {
            Listing listing = listingService.findById(id);
            listingMapper.updateFromDto(listing, listingUpdateDto);
            return ResponseEntity.ok(listingMapper.modelToDto(listingService.save(listing)));
        } catch (ServiceException e) {
            throw new ApiException("Could not update listing with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }
}
