package edu.bbte.idde.akim1784.spring.backend.dto.incoming;

import edu.bbte.idde.akim1784.spring.backend.model.Currency;

import javax.validation.constraints.Size;

public class ListingUpdateDto {
    @Size(
            min = 1,
            max = 255,
            message = "Description should be between {min} and {max} characters"
    )
    private String description;
    private Double price;
    private Currency currency;
    private Long ownerId;
    private Long propertyId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }
}
