package edu.bbte.idde.akim1784.spring.backend.dto.outgoing;

import edu.bbte.idde.akim1784.spring.backend.model.PropertyType;

import java.util.List;

public class PropertyDto {

    private Long id;
    private PropertyType type;
    private Double size;
    private List<SimpleListingDto> listings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public List<SimpleListingDto> getListings() {
        return listings;
    }

    public void setListings(List<SimpleListingDto> listings) {
        this.listings = listings;
    }
}
