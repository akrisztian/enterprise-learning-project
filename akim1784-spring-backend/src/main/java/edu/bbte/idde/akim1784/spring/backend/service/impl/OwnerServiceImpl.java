package edu.bbte.idde.akim1784.spring.backend.service.impl;

import edu.bbte.idde.akim1784.spring.backend.model.Owner;
import edu.bbte.idde.akim1784.spring.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.spring.backend.service.OwnerService;
import edu.bbte.idde.akim1784.spring.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class OwnerServiceImpl implements OwnerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OwnerService.class);
    private final OwnerRepository ownerRepository;
    private final CacheManager cacheManager;

    public OwnerServiceImpl(OwnerRepository ownerRepository, CacheManager cacheManager) {
        this.ownerRepository = ownerRepository;
        this.cacheManager = cacheManager;
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "listings", allEntries = true),
                    @CacheEvict(value = "listings-price", allEntries = true),
                    @CacheEvict(value = "owner-listings", allEntries = true),
                    @CacheEvict(value = "property-listings", allEntries = true)
            }
    )
    @Override
    public Owner save(Owner entity) {
        LOGGER.info("Save owner: {}", entity);
        // Evict listing cache
        Cache cache = cacheManager.getCache("listing-single");
        if (cache != null && entity.getListings() != null) {
            entity.getListings().stream().forEach(listing -> cache.evictIfPresent(listing.getId()));
        }
        return ownerRepository.save(entity);
    }

    @Override
    public Owner findById(Long id) {
        LOGGER.info("Find owner with ID: {}", id);
        return ownerRepository.findById(id).orElseThrow(
                () -> new ServiceException("Could not select owner with ID: " + id)
        );
    }

    @Override
    public Page<Owner> findAll(int page, int size) {
        LOGGER.info("Find {} owners on page: {}", size, page);
        return ownerRepository.findAll(PageRequest.of(page, size));
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "listings", allEntries = true),
                    @CacheEvict(value = "listings-price", allEntries = true),
                    @CacheEvict(value = "owner-listings", allEntries = true),
                    @CacheEvict(value = "property-listings", allEntries = true)
            }
    )
    @Override
    public void delete(Owner entity) {
        LOGGER.info("Delete owner: {}", entity);
        // Evict listing cache
        Cache cache = cacheManager.getCache("listing-single");
        if (cache != null && entity.getListings() != null) {
            entity.getListings().stream().forEach(listing -> cache.evictIfPresent(listing.getId()));
        }
        ownerRepository.delete(entity);
    }

    @Override
    public Page<Owner> findAllByName(String name, int page, int size) {
        LOGGER.info("Find {} owners with name like: {} on page {}", size, name, page);
        return ownerRepository.findAllByNameContainsIgnoreCase(name, PageRequest.of(page, size));
    }
}
