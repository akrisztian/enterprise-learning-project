package edu.bbte.idde.akim1784.spring.backend.service;

import org.springframework.data.domain.Page;

public interface Service<T> {
    T save(T entity);

    T findById(Long id);

    Page<T> findAll(int page, int size);

    void delete(T entity);
}
