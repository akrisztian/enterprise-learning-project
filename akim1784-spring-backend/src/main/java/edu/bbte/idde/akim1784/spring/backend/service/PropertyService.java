package edu.bbte.idde.akim1784.spring.backend.service;

import edu.bbte.idde.akim1784.spring.backend.model.Property;
import org.springframework.data.domain.Page;

public interface PropertyService extends Service<Property> {
    Page<Property> findAllBySizeBetween(Double minSize, Double maxSize, int page, int size);
}
