package edu.bbte.idde.akim1784.spring.backend.api.controller;

import edu.bbte.idde.akim1784.spring.backend.api.ApiException;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.OwnerCreationDto;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.OwnerUpdateDto;
import edu.bbte.idde.akim1784.spring.backend.dto.mapper.ListingMapper;
import edu.bbte.idde.akim1784.spring.backend.dto.mapper.OwnerMapper;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.ListingDto;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.OwnerDto;
import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import edu.bbte.idde.akim1784.spring.backend.model.Owner;
import edu.bbte.idde.akim1784.spring.backend.service.ListingService;
import edu.bbte.idde.akim1784.spring.backend.service.OwnerService;
import edu.bbte.idde.akim1784.spring.backend.service.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/owners")
public class OwnerController {

    private final OwnerService ownerService;
    private final OwnerMapper ownerMapper;
    private final ListingService listingService;
    private final ListingMapper listingMapper;

    public OwnerController(OwnerService ownerService,
                           OwnerMapper ownerMapper,
                           ListingService listingService,
                           ListingMapper listingMapper) {
        this.ownerService = ownerService;
        this.ownerMapper = ownerMapper;
        this.listingService = listingService;
        this.listingMapper = listingMapper;
    }

    @GetMapping
    public ResponseEntity<List<OwnerDto>> getAllOwnersOnPage(@RequestParam(required = false) Integer page,
                                                             @RequestParam(required = false) Integer size) {
        try {
            Page<Owner> ownerPage = ownerService.findAll(
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(ownerPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    ownerPage.stream().map(ownerMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get all Owners", e);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @GetMapping(params = "name")
    public ResponseEntity<List<OwnerDto>> getAllOwnersByName(@RequestParam String name,
                                                             @RequestParam(required = false) Integer page,
                                                             @RequestParam(required = false) Integer size) {
        try {
            Page<Owner> ownerPage = ownerService.findAllByName(
                    name,
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(ownerPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    ownerPage.stream().map(ownerMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get all Owners with name: " + name, e);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<OwnerDto> getOwnerById(@PathVariable(name = "id") Long id) {
        try {
            return ResponseEntity.ok(ownerMapper.modelToDto(ownerService.findById(id)));
        } catch (ServiceException e) {
            throw new ApiException("Could not get Owner with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/listings")
    public ResponseEntity<List<ListingDto>> getOwnerListingsById(@PathVariable(name = "id") Long id,
                                                                 @RequestParam(required = false) Integer page,
                                                                 @RequestParam(required = false) Integer size) {
        try {
            Page<Listing> listingPage = listingService.findAllByOwnerId(
                    id,
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(listingPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    listingPage.stream().map(listingMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get listings of owner with ID: " + id, e, HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @PostMapping
    public ResponseEntity<OwnerDto> createOwner(@RequestBody @Valid OwnerCreationDto ownerCreationDto) {
        try {
            Owner owner = ownerService.save(ownerMapper.incomingDtoToModel(ownerCreationDto));
            return ResponseEntity
                    .created(URI.create("owners/" + owner.getId()))
                    .body(ownerMapper.modelToDto(owner));
        } catch (ServiceException e) {
            throw new ApiException("Could not save Owner", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteOwner(@PathVariable(name = "id") Long id) {
        try {
            ownerService.delete(ownerService.findById(id));
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            throw new ApiException("No owner with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<OwnerDto> updateOwner(@PathVariable(name = "id") Long id,
                                                @RequestBody @Valid OwnerUpdateDto ownerUpdateDto) {
        try {
            Owner owner = ownerService.findById(id);
            ownerMapper.updateFromDto(owner, ownerUpdateDto);
            return ResponseEntity.ok(ownerMapper.modelToDto(ownerService.save(owner)));
        } catch (ServiceException e) {
            throw new ApiException("Could not update owner with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }
}
