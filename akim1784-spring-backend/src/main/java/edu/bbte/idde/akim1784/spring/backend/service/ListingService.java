package edu.bbte.idde.akim1784.spring.backend.service;

import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import org.springframework.data.domain.Page;

public interface ListingService extends Service<Listing> {
    Page<Listing> findAllByPriceBetween(Double minPrice, Double maxPrice, int page, int size);

    Page<Listing> findAllByOwnerId(Long id, int page, int size);

    Page<Listing> findAllByPropertyId(Long id, int page, int size);
}
