package edu.bbte.idde.akim1784.spring.backend.api.controller;

import edu.bbte.idde.akim1784.spring.backend.api.ApiException;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.PropertyCreationDto;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.PropertyUpdateDto;
import edu.bbte.idde.akim1784.spring.backend.dto.mapper.ListingMapper;
import edu.bbte.idde.akim1784.spring.backend.dto.mapper.PropertyMapper;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.ListingDto;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.PropertyDto;
import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import edu.bbte.idde.akim1784.spring.backend.model.Property;
import edu.bbte.idde.akim1784.spring.backend.service.ListingService;
import edu.bbte.idde.akim1784.spring.backend.service.PropertyService;
import edu.bbte.idde.akim1784.spring.backend.service.ServiceException;
import edu.bbte.idde.akim1784.spring.backend.util.NumberConverter;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/properties")
public class PropertyController {

    private final PropertyService propertyService;
    private final PropertyMapper propertyMapper;
    private final ListingService listingService;
    private final ListingMapper listingMapper;

    public PropertyController(PropertyService propertyService,
                              PropertyMapper propertyMapper,
                              ListingService listingService,
                              ListingMapper listingMapper) {
        this.propertyService = propertyService;
        this.propertyMapper = propertyMapper;
        this.listingService = listingService;
        this.listingMapper = listingMapper;
    }

    @GetMapping(params = {"!minSize", "!maxSize"})
    public ResponseEntity<List<PropertyDto>> getAllPropertiesOnPage(@RequestParam(required = false) Integer page,
                                                                    @RequestParam(required = false) Integer size) {
        try {
            Page<Property> propertyPage = propertyService.findAll(
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(propertyPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    propertyPage.stream().map(propertyMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get all Properties", e);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @GetMapping
    public ResponseEntity<List<PropertyDto>> getAllPropertiesBySizeBetween(
            @RequestParam Map<String, String> sizes,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        try {
            Page<Property> propertyPage = propertyService.findAllBySizeBetween(
                    NumberConverter.stringToDoubleOrNull(sizes.get("minSize")),
                    NumberConverter.stringToDoubleOrNull(sizes.get("maxSize")),
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(propertyPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    propertyPage.stream().map(propertyMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get all Properties", e);
        } catch (NumberFormatException e) {
            throw new ApiException("Sizes should be valid real numbers", e);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<PropertyDto> getPropertyById(@PathVariable(name = "id") Long id) {
        try {
            return ResponseEntity.ok(propertyMapper.modelToDto(propertyService.findById(id)));
        } catch (ServiceException e) {
            throw new ApiException("Could not get Property with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/listings")
    public ResponseEntity<List<ListingDto>> getPropertyListingsById(@PathVariable(name = "id") Long id,
                                                                    @RequestParam(required = false) Integer page,
                                                                    @RequestParam(required = false) Integer size) {
        try {
            Page<Listing> listingPage = listingService.findAllByPropertyId(
                    id,
                    page == null ? 0 : page - 1,
                    size == null ? 15 : size
            );
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("X-Total-Count", String.valueOf(listingPage.getTotalElements()));
            return ResponseEntity.ok().headers(responseHeaders).body(
                    listingPage.stream().map(listingMapper::modelToDto).collect(Collectors.toList())
            );
        } catch (ServiceException e) {
            throw new ApiException("Could not get listings of property with ID: " + id, e, HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            throw new ApiException("Invalid page request parameters", e);
        }
    }

    @PostMapping
    public ResponseEntity<PropertyDto> createProperty(@RequestBody @Valid PropertyCreationDto propertyCreationDto) {
        try {
            Property property = propertyService.save(propertyMapper.incomingDtoToModel(propertyCreationDto));
            return ResponseEntity
                    .created(URI.create("properties/" + property.getId()))
                    .body(propertyMapper.modelToDto(property));
        } catch (ServiceException e) {
            throw new ApiException("Could not save Property", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteProperty(@PathVariable(name = "id") Long id) {
        try {
            propertyService.delete(propertyService.findById(id));
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            throw new ApiException("No property with ID: " + id, e, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<PropertyDto> updateProperty(@PathVariable(name = "id") Long id,
                                                      @RequestBody @Valid PropertyUpdateDto propertyUpdateDto) {
        try {
            Property property = propertyService.findById(id);
            propertyMapper.updateFromDto(property, propertyUpdateDto);
            return ResponseEntity.ok(propertyMapper.modelToDto(propertyService.save(property)));
        } catch (ServiceException e) {
            throw new ApiException("Could not update property with ID: " + id, e);
        }
    }
}
