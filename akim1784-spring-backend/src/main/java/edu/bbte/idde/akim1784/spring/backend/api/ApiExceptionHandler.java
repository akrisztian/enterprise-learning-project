package edu.bbte.idde.akim1784.spring.backend.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<Object> handleApiException(ApiException ex, WebRequest request) {
        LOGGER.error(ex.getMessage());
        List<String> causes = new LinkedList<>();
        Throwable exception = ex;

        do {
            causes.add(exception.getMessage());
            exception = exception.getCause();
        } while (exception != null);

        return handleExceptionInternal(ex, Collections.singletonMap("causes", causes), new HttpHeaders(),
                ex.getStatus(), request);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
        LOGGER.error(ex.getMessage());
        List<String> causes = new LinkedList<>();

        ex.getConstraintViolations()
                .stream()
                .forEach(error -> causes.add(error.getPropertyPath() + ": " + error.getMessage()));

        return handleExceptionInternal(ex, Collections.singletonMap("causes", causes), new HttpHeaders(),
                HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        LOGGER.error(ex.getMessage());
        List<String> causes = new LinkedList<>();
        ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(error -> causes.add(error.getField() + ": " + error.getDefaultMessage()));

        ex.getBindingResult()
                .getGlobalErrors()
                .stream()
                .forEach(error -> causes.add(error.getObjectName() + ": " + error.getDefaultMessage()));

        return handleExceptionInternal(ex, Collections.singletonMap("causes", causes), headers,
                HttpStatus.BAD_REQUEST, request);
    }

}
