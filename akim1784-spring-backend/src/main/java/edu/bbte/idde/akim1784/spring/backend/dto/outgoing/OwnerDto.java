package edu.bbte.idde.akim1784.spring.backend.dto.outgoing;

import java.util.List;

public class OwnerDto {

    private Long id;
    private String name;
    private List<SimpleListingDto> listings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SimpleListingDto> getListings() {
        return listings;
    }

    public void setListings(List<SimpleListingDto> listings) {
        this.listings = listings;
    }
}
