package edu.bbte.idde.akim1784.spring.backend.repository;

import edu.bbte.idde.akim1784.spring.backend.model.Listing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListingRepository extends JpaRepository<Listing, Long> {
    Page<Listing> findAllByPriceBetween(Double minPrice, Double maxPrice, Pageable pageable);

    Page<Listing> findAllByPriceGreaterThanEqual(Double price, Pageable pageable);

    Page<Listing> findAllByPriceLessThanEqual(Double price, Pageable pageable);

    Page<Listing> findAllByOwnerId(Long id, Pageable pageable);

    Page<Listing> findAllByPropertyId(Long id, Pageable pageable);
}
