package edu.bbte.idde.akim1784.spring.backend.repository;

import edu.bbte.idde.akim1784.spring.backend.model.Property;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Long> {
    Page<Property> findAllBySizeBetween(Double minSize, Double maxSize, Pageable pageable);

    Page<Property> findAllBySizeGreaterThanEqual(Double size, Pageable pageable);

    Page<Property> findAllBySizeLessThanEqual(Double size, Pageable pageable);
}
