package edu.bbte.idde.akim1784.spring.backend.dto.incoming;

import javax.validation.constraints.Size;

public class OwnerUpdateDto {
    @Size(
            min = 1,
            max = 255,
            message = "Name should be between {min} and {max} characters!"
    )
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
