package edu.bbte.idde.akim1784.spring.backend.dto.mapper;

import edu.bbte.idde.akim1784.spring.backend.dto.incoming.PropertyCreationDto;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.PropertyUpdateDto;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.PropertyDto;
import edu.bbte.idde.akim1784.spring.backend.model.Property;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PropertyMapper {
    private final ModelMapper mapper;

    public PropertyMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Property incomingDtoToModel(PropertyCreationDto propertyCreationDto) {
        return mapper.map(propertyCreationDto, Property.class);
    }

    public PropertyDto modelToDto(Property property) {
        return mapper.map(property, PropertyDto.class);
    }

    public void updateFromDto(Property property, PropertyUpdateDto propertyUpdateDto) {
        mapper.map(propertyUpdateDto, property);
    }
}
