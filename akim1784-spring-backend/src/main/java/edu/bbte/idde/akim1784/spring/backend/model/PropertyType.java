package edu.bbte.idde.akim1784.spring.backend.model;

public enum PropertyType {
    FIELD,
    HOUSE,
    APARTMENT
}
