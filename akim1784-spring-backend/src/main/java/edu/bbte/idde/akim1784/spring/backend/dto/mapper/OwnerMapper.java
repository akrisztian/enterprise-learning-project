package edu.bbte.idde.akim1784.spring.backend.dto.mapper;

import edu.bbte.idde.akim1784.spring.backend.dto.incoming.OwnerCreationDto;
import edu.bbte.idde.akim1784.spring.backend.dto.incoming.OwnerUpdateDto;
import edu.bbte.idde.akim1784.spring.backend.dto.outgoing.OwnerDto;
import edu.bbte.idde.akim1784.spring.backend.model.Owner;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OwnerMapper {
    private final ModelMapper mapper;

    public OwnerMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Owner incomingDtoToModel(OwnerCreationDto ownerCreationDto) {
        return mapper.map(ownerCreationDto, Owner.class);
    }

    public OwnerDto modelToDto(Owner owner) {
        return mapper.map(owner, OwnerDto.class);
    }

    public void updateFromDto(Owner owner, OwnerUpdateDto ownerUpdateDto) {
        mapper.map(ownerUpdateDto, owner);
    }
}
