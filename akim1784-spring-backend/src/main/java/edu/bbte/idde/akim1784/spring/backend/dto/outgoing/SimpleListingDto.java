package edu.bbte.idde.akim1784.spring.backend.dto.outgoing;

import edu.bbte.idde.akim1784.spring.backend.model.Currency;

import java.time.LocalDate;

public class SimpleListingDto {

    private Long id;
    private String description;
    private Double price;
    private Currency currency;
    private LocalDate date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
