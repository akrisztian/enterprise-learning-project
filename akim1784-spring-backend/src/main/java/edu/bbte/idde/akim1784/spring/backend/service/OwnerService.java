package edu.bbte.idde.akim1784.spring.backend.service;

import edu.bbte.idde.akim1784.spring.backend.model.Owner;
import org.springframework.data.domain.Page;

public interface OwnerService extends Service<Owner> {
    Page<Owner> findAllByName(String name, int page, int size);
}
