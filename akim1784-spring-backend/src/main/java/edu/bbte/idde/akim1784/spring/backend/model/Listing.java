package edu.bbte.idde.akim1784.spring.backend.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Listing extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    private Property property;
    @ManyToOne(fetch = FetchType.EAGER)
    private Owner owner;
    @Basic
    private String description;
    @Basic
    private Double price;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    @CreatedDate
    private LocalDate date;

    public Listing() {
        super();
    }

    public Listing(Property property,
                   Owner owner,
                   String description,
                   Double price,
                   Currency currency,
                   LocalDate date) {
        super();
        this.property = property;
        this.owner = owner;
        this.description = description;
        this.price = price;
        this.currency = currency;
        this.date = date;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Listing{");
        sb.append("property=").append(property);
        sb.append(", owner=").append(owner);
        sb.append(", description='").append(description).append('\'');
        sb.append(", price=").append(price);
        sb.append(", currency=").append(currency);
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }
}
