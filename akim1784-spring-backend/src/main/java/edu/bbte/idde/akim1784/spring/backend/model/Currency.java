package edu.bbte.idde.akim1784.spring.backend.model;

public enum Currency {
    RON,
    DOLLAR,
    EURO,
    STERLING;
}
