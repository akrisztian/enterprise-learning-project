package edu.bbte.idde.akim1784.spring.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Property extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "property_type")
    private PropertyType type;
    @Basic
    private Double size;
    @OneToMany(mappedBy = "property",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Listing> listings;

    public Property() {
        super();
    }

    public Property(PropertyType type, Double size) {
        super();
        this.type = type;
        this.size = size;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Property{");
        sb.append("type=").append(type);
        sb.append(", size=").append(size);
        sb.append('}');
        return sb.toString();
    }
}
