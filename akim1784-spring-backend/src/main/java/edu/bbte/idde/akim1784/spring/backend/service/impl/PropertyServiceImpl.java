package edu.bbte.idde.akim1784.spring.backend.service.impl;

import edu.bbte.idde.akim1784.spring.backend.model.Property;
import edu.bbte.idde.akim1784.spring.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.spring.backend.service.PropertyService;
import edu.bbte.idde.akim1784.spring.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class PropertyServiceImpl implements PropertyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyService.class);
    private final PropertyRepository propertyRepository;
    private final CacheManager cacheManager;

    public PropertyServiceImpl(PropertyRepository propertyRepository, CacheManager cacheManager) {
        this.propertyRepository = propertyRepository;
        this.cacheManager = cacheManager;
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "listings", allEntries = true),
                    @CacheEvict(value = "listings-price", allEntries = true),
                    @CacheEvict(value = "owner-listings", allEntries = true),
                    @CacheEvict(value = "property-listings", allEntries = true)
            }
    )
    @Override
    public Property save(Property entity) {
        LOGGER.info("Save property: {}", entity);
        // Evict listing cache
        Cache cache = cacheManager.getCache("listing-single");
        if (cache != null && entity.getListings() != null) {
            entity.getListings().stream().forEach(listing -> cache.evictIfPresent(listing.getId()));
        }
        return propertyRepository.save(entity);
    }

    @Override
    public Property findById(Long id) {
        LOGGER.info("Find property with ID: {}", id);
        return propertyRepository.findById(id).orElseThrow(
                () -> new ServiceException("Could not select property with ID: " + id)
        );
    }

    @Override
    public Page<Property> findAll(int page, int size) {
        LOGGER.info("Find {} properties on page: {}", size, page);
        return propertyRepository.findAll(PageRequest.of(page, size));
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "listings", allEntries = true),
                    @CacheEvict(value = "listings-price", allEntries = true),
                    @CacheEvict(value = "owner-listings", allEntries = true),
                    @CacheEvict(value = "property-listings", allEntries = true)
            }
    )
    @Override
    public void delete(Property entity) {
        LOGGER.info("Delete property: {}", entity);
        // Evict listing cache
        Cache cache = cacheManager.getCache("listing-single");
        if (cache != null && entity.getListings() != null) {
            entity.getListings().stream().forEach(listing -> cache.evictIfPresent(listing.getId()));
        }
        propertyRepository.delete(entity);
    }

    @Override
    public Page<Property> findAllBySizeBetween(Double minSize, Double maxSize, int page, int size) {
        LOGGER.info("Find {} properties with size between: {} and {} on page {}", size, minSize, maxSize, page);
        PageRequest pageRequest = PageRequest.of(page, size);
        if (minSize == null && maxSize == null) {
            return propertyRepository.findAll(pageRequest);
        }

        if (minSize == null) {
            return propertyRepository.findAllBySizeLessThanEqual(maxSize, pageRequest);
        }

        if (maxSize == null) {
            return propertyRepository.findAllBySizeGreaterThanEqual(minSize, pageRequest);
        }

        return propertyRepository.findAllBySizeBetween(minSize, maxSize, pageRequest);
    }
}
