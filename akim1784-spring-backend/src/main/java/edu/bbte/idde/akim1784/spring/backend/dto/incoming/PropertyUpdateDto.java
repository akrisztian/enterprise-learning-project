package edu.bbte.idde.akim1784.spring.backend.dto.incoming;

import edu.bbte.idde.akim1784.spring.backend.model.PropertyType;

public class PropertyUpdateDto {
    private PropertyType type;
    private Double size;

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }
}
