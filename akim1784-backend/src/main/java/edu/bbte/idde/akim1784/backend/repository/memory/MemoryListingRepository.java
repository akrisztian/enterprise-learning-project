package edu.bbte.idde.akim1784.backend.repository.memory;

import edu.bbte.idde.akim1784.backend.model.Currency;
import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Owner;
import edu.bbte.idde.akim1784.backend.model.Property;
import edu.bbte.idde.akim1784.backend.repository.ListingRepository;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemoryListingRepository implements ListingRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemoryListingRepository.class);
    private static final Map<Long, Listing> LISTINGS = new ConcurrentHashMap<>();
    private static final AtomicLong ATOMIC_ID = new AtomicLong();
    private final OwnerRepository ownerRepository;
    private final PropertyRepository propertyRepository;

    public MemoryListingRepository(OwnerRepository ownerRepository, PropertyRepository propertyRepository) {
        this.ownerRepository = ownerRepository;
        this.propertyRepository = propertyRepository;
        LOGGER.warn("Mocking listing data");

        // mock some data
        Owner owner1 = this.ownerRepository.findById(1L);
        Owner owner2 = this.ownerRepository.findById(2L);

        Property property1 = this.propertyRepository.findById(1L);
        Property property2 = this.propertyRepository.findById(2L);

        for (int i = 0; i < 5; i++) {
            create(new Listing(
                    property1, owner1, "The first", 500D, Currency.DOLLAR, LocalDate.now()
            ));

            create(new Listing(
                    property2, owner2, "The second", 550D, Currency.EURO, LocalDate.now()
            ));
        }
    }

    @Override
    public Listing create(Listing entity) {
        createNestedIfNotExists(entity);
        entity.setId(ATOMIC_ID.incrementAndGet());
        LISTINGS.put(entity.getId(), entity);
        LOGGER.info("create listing with id {}", entity.getId());
        return entity;
    }

    @Override
    public void replace(Listing entity) {
        LOGGER.info("replace listing with id {}", entity.getId());
        if (entity.getCurrency() == null
                || entity.getProperty() == null
                || entity.getPrice() == null
                || entity.getOwner() == null
                || entity.getDescription() == null
                || entity.getDate() == null) {
            LOGGER.error("replace() called with incomplete object {}", entity);
            throw new RepositoryException("Entity is not complete");
        }

        Listing originalListing = LISTINGS.get(entity.getId());

        originalListing.setCurrency(entity.getCurrency());
        originalListing.setDate(entity.getDate());
        originalListing.setDescription(entity.getDescription());
        originalListing.setOwner(entity.getOwner());
        originalListing.setPrice(entity.getPrice());
        originalListing.setProperty(entity.getProperty());
        createNestedIfNotExists(originalListing);
    }

    @Override
    public void update(Listing entity) {
        LOGGER.info("update listing with id {}", entity.getId());
        Listing originalListing = LISTINGS.get(entity.getId());

        if (entity.getCurrency() != null) {
            originalListing.setCurrency(entity.getCurrency());
        }
        if (entity.getDate() != null) {
            originalListing.setDate(entity.getDate());
        }
        if (entity.getDescription() != null) {
            originalListing.setDescription(entity.getDescription());
        }
        if (entity.getOwner() != null) {
            originalListing.setOwner(entity.getOwner());
        }
        if (entity.getPrice() != null) {
            originalListing.setPrice(entity.getPrice());
        }
        if (entity.getProperty() != null) {
            originalListing.setProperty(entity.getProperty());
        }

        createNestedIfNotExists(originalListing);
    }

    @Override
    public void delete(Listing entity) {
        LOGGER.info("delete listing with id {}", entity.getId());
        LISTINGS.remove(entity.getId());
    }

    @Override
    public Collection<Listing> findAll() {
        LOGGER.info("find all listings");
        return LISTINGS.values();
    }

    @Override
    public Listing findById(Long id) {
        LOGGER.info("find listing with id {}", id);
        if (LISTINGS.containsKey(id)) {
            return LISTINGS.get(id);
        } else {
            LOGGER.error("no listing with id: {}", id);
            throw new RepositoryException("No Listing with ID: " + id);
        }
    }

    private void createNestedIfNotExists(Listing entity) {
        if (entity.getOwner() != null && entity.getOwner().getId() == null) {
            entity.setOwner(this.ownerRepository.create(entity.getOwner()));
        }

        if (entity.getProperty() != null && entity.getProperty().getId() == null) {
            entity.setProperty(this.propertyRepository.create(entity.getProperty()));
        }
    }

    @Override
    public Collection<Listing> findAllListingsInPriceInterval(Double minPrice, Double maxPrice) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findAllListingsByOwnerName(String name) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findAllListingsWithFieldProperty() {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }
}
