package edu.bbte.idde.akim1784.backend.repository.jdbc;

import edu.bbte.idde.akim1784.backend.repository.ListingRepository;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryFactory;

public class JdbcRepositoryFactory extends RepositoryFactory {

    private static OwnerRepository ownerRepository;
    private static PropertyRepository propertyRepository;
    private static ListingRepository listingRepository;

    @Override
    public synchronized ListingRepository getListingRepository() {
        if (listingRepository == null) {
            listingRepository = new JdbcListingRepository(getOwnerRepository(), getPropertyRepository());
        }
        return listingRepository;
    }

    @Override
    public synchronized OwnerRepository getOwnerRepository() {
        if (ownerRepository == null) {
            ownerRepository = new JdbcOwnerRepository();
        }
        return ownerRepository;
    }

    @Override
    public synchronized PropertyRepository getPropertyRepository() {
        if (propertyRepository == null) {
            propertyRepository = new JdbcPropertyRepository();
        }
        return propertyRepository;
    }
}
