package edu.bbte.idde.akim1784.backend.repository.jdbc;

import edu.bbte.idde.akim1784.backend.config.ConfigurationFactory;
import edu.bbte.idde.akim1784.backend.model.Currency;
import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.repository.ListingRepository;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import edu.bbte.idde.akim1784.backend.repository.util.ConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class JdbcListingRepository implements ListingRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListingRepository.class);
    private final OwnerRepository ownerRepository;
    private final PropertyRepository propertyRepository;

    public JdbcListingRepository(OwnerRepository ownerRepository, PropertyRepository propertyRepository) {
        this.ownerRepository = ownerRepository;
        this.propertyRepository = propertyRepository;

        if (ConfigurationFactory.getConfiguration().getJdbcConfiguration().getCreateTables()) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table if not exists listing(\n");
            sb.append("id serial primary key not null,\n");
            sb.append("uuid varchar(512) not null, \n");
            sb.append("description varchar(2048), \n");
            sb.append("price real, \n");
            sb.append("currency varchar(10), \n");
            sb.append("date date, \n");
            sb.append("owner_id int references owner(id), \n");
            sb.append("property_id int references property(id)); \n");

            try (Connection connection = ConnectionManager.getConnection();
                    Statement statement = connection.createStatement()) {
                statement.executeUpdate(sb.toString());
            } catch (SQLException e) {
                LOGGER.error("SQL Error");
                throw new RepositoryException("SQL Error", e);
            }
        }
    }

    @Override
    public Listing create(Listing entity) {
        createNestedIfNotExists(entity);

        String query =
                "insert into listing(uuid, description, price, currency, date, owner_id, property_id)"
                        + "values (?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement =
                        connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getUuid().toString());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setDouble(3, entity.getPrice());
            preparedStatement.setString(4, entity.getCurrency().toString());
            preparedStatement.setObject(5, entity.getDate());
            preparedStatement.setLong(6, entity.getOwner().getId());
            preparedStatement.setLong(7, entity.getProperty().getId());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            } else {
                generatedKeys.close();
                LOGGER.error("Could not insert entity into listing table");
                throw new RepositoryException("Could not insert entity into listing table");
            }
            generatedKeys.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        LOGGER.info("create listing with id {}", entity.getId());
        return entity;
    }

    @Override
    public void replace(Listing entity) {
        LOGGER.info("replace listing with id {}", entity.getId());
        if (entity.getCurrency() == null
                || entity.getProperty() == null
                || entity.getPrice() == null
                || entity.getOwner() == null
                || entity.getDescription() == null
                || entity.getDate() == null) {
            LOGGER.error("replace() called with incomplete object {}", entity);
            throw new RepositoryException("Entity is not complete");
        }
        update(entity);
    }

    @Override
    public void update(Listing entity) {
        LOGGER.info("update listing with id {}", entity.getId());
        createNestedIfNotExists(entity);

        String query =
                "update listing set description=?, price=?, currency=?, date=?, owner_id=?, property_id=? where id=?";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, entity.getDescription());
            preparedStatement.setDouble(2, entity.getPrice());
            preparedStatement.setString(3, entity.getCurrency().toString());
            preparedStatement.setObject(4, entity.getDate());
            preparedStatement.setLong(5, entity.getOwner().getId());
            preparedStatement.setLong(6, entity.getProperty().getId());
            preparedStatement.setLong(7, entity.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                LOGGER.error("Could not update listing with id {}", entity.getId());
                throw new RepositoryException("Could not update listing with id " + entity.getId());
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
    }

    @Override
    public void delete(Listing entity) {
        LOGGER.info("delete listing with id {}", entity.getId());
        String query = "delete from listing where id=?";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, entity.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows < 1) {
                LOGGER.error("Could not delete listing with id {}", entity.getId());
                throw new RepositoryException("Could not delete listing with id" + entity.getId());
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
    }

    @Override
    public Collection<Listing> findAll() {
        LOGGER.info("find all listings");
        List<Listing> listings = new LinkedList<>();
        String query = "select * from listing";

        try (Connection connection = ConnectionManager.getConnection();
                Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                listings.add(resultSetToListing(resultSet));
            }

            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }

        return listings;
    }

    @Override
    public Listing findById(Long id) {
        LOGGER.info("find Listing with id {}", id);
        if (id == null) {
            return null;
        }

        Listing entity;
        String query = "select * from listing where id=?";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                entity = resultSetToListing(resultSet);
            } else {
                LOGGER.error("Could not find listing with id: {}", id);
                throw new RepositoryException("Could not find listing with id: " + id);
            }
            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return entity;
    }

    private void createNestedIfNotExists(Listing entity) {
        if (entity.getOwner() != null && entity.getOwner().getId() == null) {
            entity.setOwner(this.ownerRepository.create(entity.getOwner()));
        }

        if (entity.getProperty() != null && entity.getProperty().getId() == null) {
            entity.setProperty(this.propertyRepository.create(entity.getProperty()));
        }
    }

    private Listing resultSetToListing(ResultSet resultSet) throws SQLException {
        Listing listing = new Listing();
        listing.setUuid(UUID.fromString(resultSet.getString("uuid")));
        listing.setId(resultSet.getLong("id"));
        listing.setDescription(resultSet.getString("description"));
        listing.setPrice(resultSet.getDouble("price"));
        listing.setCurrency(Currency.valueOf(resultSet.getString("currency")));
        listing.setDate(resultSet.getObject("date", LocalDate.class));
        listing.setProperty(this.propertyRepository.findById(resultSet.getLong("property_id")));
        listing.setOwner(this.ownerRepository.findById(resultSet.getLong("owner_id")));

        return listing;
    }

    @Override
    public Collection<Listing> findAllListingsInPriceInterval(Double minPrice, Double maxPrice) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findAllListingsByOwnerName(String name) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findAllListingsWithFieldProperty() {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }
}
