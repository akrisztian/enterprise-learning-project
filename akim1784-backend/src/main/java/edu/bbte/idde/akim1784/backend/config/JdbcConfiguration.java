package edu.bbte.idde.akim1784.backend.config;

public class JdbcConfiguration {
    private String url;
    private String user;
    private String password;
    private String driverClassName;
    private Integer poolSize = 1;
    private Boolean createTables = false;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public Integer getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(Integer poolSize) {
        this.poolSize = poolSize;
    }

    public Boolean getCreateTables() {
        return createTables;
    }

    public void setCreateTables(Boolean createTables) {
        this.createTables = createTables;
    }
}
