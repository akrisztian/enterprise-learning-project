package edu.bbte.idde.akim1784.backend.config;

public class RepositoryConfiguration {
    private RepositoryType type = RepositoryType.MEMORY;

    public RepositoryType getType() {
        return type;
    }

    public void setType(RepositoryType type) {
        this.type = type;
    }
}
