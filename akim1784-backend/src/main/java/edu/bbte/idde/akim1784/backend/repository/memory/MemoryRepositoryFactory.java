package edu.bbte.idde.akim1784.backend.repository.memory;

import edu.bbte.idde.akim1784.backend.repository.ListingRepository;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryFactory;

public class MemoryRepositoryFactory extends RepositoryFactory {

    private static OwnerRepository ownerRepository;
    private static PropertyRepository propertyRepository;
    private static ListingRepository listingRepository;

    @Override
    public synchronized ListingRepository getListingRepository() {
        if (listingRepository == null) {
            listingRepository = new MemoryListingRepository(getOwnerRepository(), getPropertyRepository());
        }
        return listingRepository;
    }

    @Override
    public synchronized OwnerRepository getOwnerRepository() {
        if (ownerRepository == null) {
            ownerRepository = new MemoryOwnerRepository();
        }
        return ownerRepository;
    }

    @Override
    public synchronized PropertyRepository getPropertyRepository() {
        if (propertyRepository == null) {
            propertyRepository = new MemoryPropertyRepository();
        }
        return propertyRepository;
    }
}
