package edu.bbte.idde.akim1784.backend.repository;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Property;

import java.util.Collection;

public interface PropertyRepository extends Repository<Property> {
    Collection<Property> findAllPropertiesBySizeInterval(Double minSize, Double maxSize);

    Collection<Listing> findPropertyListingsById(Long id);
}
