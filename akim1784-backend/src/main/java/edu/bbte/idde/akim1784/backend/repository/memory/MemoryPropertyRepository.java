package edu.bbte.idde.akim1784.backend.repository.memory;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Property;
import edu.bbte.idde.akim1784.backend.model.PropertyType;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemoryPropertyRepository implements PropertyRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemoryPropertyRepository.class);
    private static final Map<Long, Property> PROPERTIES = new ConcurrentHashMap<>();
    private static final AtomicLong ATOMIC_ID = new AtomicLong();

    public MemoryPropertyRepository() {
        LOGGER.warn("Mocking property data");

        create(new Property(PropertyType.APARTMENT, 5000D));
        create(new Property(PropertyType.FIELD, 65000D));
    }

    @Override
    public Property create(Property entity) {
        entity.setId(ATOMIC_ID.incrementAndGet());
        LOGGER.info("create property with id {}", entity.getId());
        PROPERTIES.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void replace(Property entity) {
        LOGGER.info("replace entity with id {}", entity.getId());
        if (entity.getSize() == null || entity.getType() == null) {
            LOGGER.error("replace() called with incomplete object {}", entity);
            throw new RepositoryException("Entity is not complete");
        }

        Property originalProperty = PROPERTIES.get(entity.getId());

        originalProperty.setSize(entity.getSize());
        originalProperty.setType(entity.getType());
    }

    @Override
    public void update(Property entity) {
        LOGGER.info("update entity with id {}", entity.getId());

        Property originalProperty = PROPERTIES.get(entity.getId());

        originalProperty.setSize(entity.getSize());
        originalProperty.setType(entity.getType());
    }

    @Override
    public void delete(Property entity) {
        LOGGER.info("delete property with id {}", entity.getId());
        PROPERTIES.remove(entity.getId());
    }

    @Override
    public Collection<Property> findAll() {
        LOGGER.info("find all properties");
        return PROPERTIES.values();
    }

    @Override
    public Property findById(Long id) {
        LOGGER.info("find property with id {}", id);
        if (PROPERTIES.containsKey(id)) {
            return PROPERTIES.get(id);
        } else {
            throw new RepositoryException("No property with ID: " + id);
        }
    }

    @Override
    public Collection<Property> findAllPropertiesBySizeInterval(Double minSize, Double maxSize) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findPropertyListingsById(Long id) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }
}
