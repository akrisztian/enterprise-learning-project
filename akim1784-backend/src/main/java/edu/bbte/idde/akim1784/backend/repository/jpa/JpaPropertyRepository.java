package edu.bbte.idde.akim1784.backend.repository.jpa;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Property;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;

import javax.persistence.TypedQuery;
import java.util.Collection;

public class JpaPropertyRepository extends JpaRepository<Property> implements PropertyRepository {
    public JpaPropertyRepository() {
        super(Property.class);
    }

    @Override
    public Collection<Property> findAllPropertiesBySizeInterval(Double minSize, Double maxSize) {
        logger.info("Find all Properties by size interval");
        TypedQuery<Property> typedQuery = entityManager
                .createQuery("select p from Property p where p.size between :min and :max", Property.class);
        typedQuery.setParameter("min", minSize);
        typedQuery.setParameter("max", maxSize);
        return typedQuery.getResultList();
    }

    @Override
    public Collection<Listing> findPropertyListingsById(Long id) {
        logger.info("find listing property listings by id");
        return findById(id).getListings();
    }
}
