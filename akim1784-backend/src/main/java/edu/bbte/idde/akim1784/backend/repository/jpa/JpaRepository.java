package edu.bbte.idde.akim1784.backend.repository.jpa;

import edu.bbte.idde.akim1784.backend.repository.Repository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;

public abstract class JpaRepository<T> implements Repository<T> {

    protected final Logger logger;
    private final Class<T> genericClass;
    protected EntityManager entityManager;

    public JpaRepository(Class<T> genericClass) {
        this.genericClass = genericClass;
        this.logger = LoggerFactory.getLogger(genericClass);
        entityManager = Persistence.createEntityManagerFactory("akim1784-idde").createEntityManager();
    }

    @Override
    public T create(T entity) {
        logger.info("Create " + genericClass.getSimpleName());
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.persist(entity);
            transaction.commit();
        } catch (PersistenceException ex) {
            logger.error("Could not persist {} {}", genericClass.getSimpleName(), entity);
            transaction.rollback();
            throw new RepositoryException("Could not persist " + genericClass.getSimpleName(), ex);
        }
        return entity;
    }

    @Override
    public void replace(T entity) {
        logger.info("Replace " + genericClass.getSimpleName());
        Arrays.stream(entity.getClass().getDeclaredFields()).forEach(field -> {
            try {
                field.setAccessible(true);
                if (field.get(entity) == null) {
                    throw new RepositoryException("Replace called on incomplete object " + entity);
                }
            } catch (IllegalAccessException e) {
                logger.error("Could not get {} value of {} {}", field.getName(), genericClass.getSimpleName(), entity);
                throw new RepositoryException("Could not get field value.", e);
            }
        });
        update(entity);
    }

    @Override
    public void update(T entity) {
        logger.info("Update " + genericClass.getSimpleName());
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.merge(entity);
            transaction.commit();
        } catch (PersistenceException ex) {
            logger.error("Could not update {} {}", genericClass.getSimpleName(), entity);
            transaction.rollback();
            throw new RepositoryException("Could not update " + genericClass.getSimpleName(), ex);
        }
    }

    @Override
    public void delete(T entity) {
        logger.info("Delete " + genericClass.getSimpleName());
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.remove(entity);
            transaction.commit();
        } catch (PersistenceException ex) {
            logger.error("Could not delete {} {}", genericClass.getSimpleName(), entity);
            transaction.rollback();
            throw new RepositoryException("Could not delete " + genericClass.getSimpleName(), ex);
        }
    }

    @Override
    public Collection<T> findAll() {
        logger.info("Find all {} entities", genericClass.getSimpleName());
        TypedQuery<T> typedQuery = entityManager.createQuery("from " + genericClass.getSimpleName(), genericClass);
        return typedQuery.getResultList();
    }

    @Override
    public T findById(Long id) {
        logger.info("Find {}", genericClass.getSimpleName());
        T entity = entityManager.find(genericClass, id);
        if (entity == null) {
            throw new RepositoryException("Could not find " + genericClass.getSimpleName() + "with ID: " + id,
                    new EntityNotFoundException());
        } else {
            return entity;
        }
    }
}
