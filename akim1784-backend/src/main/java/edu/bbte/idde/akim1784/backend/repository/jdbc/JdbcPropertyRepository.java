package edu.bbte.idde.akim1784.backend.repository.jdbc;

import edu.bbte.idde.akim1784.backend.config.ConfigurationFactory;
import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Property;
import edu.bbte.idde.akim1784.backend.model.PropertyType;
import edu.bbte.idde.akim1784.backend.repository.PropertyRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import edu.bbte.idde.akim1784.backend.repository.util.ConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class JdbcPropertyRepository implements PropertyRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcPropertyRepository.class);

    public JdbcPropertyRepository() {
        if (ConfigurationFactory.getConfiguration().getJdbcConfiguration().getCreateTables()) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table if not exists property(\n");
            sb.append("id serial primary key not null,\n");
            sb.append("uuid varchar(512) not null, \n");
            sb.append("property_type varchar(256), \n");
            sb.append("size real );");

            try (Connection connection = ConnectionManager.getConnection();
                    Statement statement = connection.createStatement()) {
                statement.executeUpdate(sb.toString());
            } catch (SQLException e) {
                LOGGER.error("SQL Error");
                throw new RepositoryException("SQL Error", e);
            }
        }
    }

    @Override
    public Property create(Property entity) {
        String query = "insert into property(uuid, property_type, size) values (?,?,?)";
        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement =
                        connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getUuid().toString());
            preparedStatement.setString(2, entity.getType().toString());
            preparedStatement.setDouble(3, entity.getSize());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            } else {
                LOGGER.error("Could not insert entity into property table");
                throw new RepositoryException("Could not insert entity into property table");
            }
            generatedKeys.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return entity;
    }

    @Override
    public void replace(Property entity) {
        if (entity.getType() == null) {
            LOGGER.error("replace called with incomplete object {}", entity);
            throw new RepositoryException("replace called with incomplete object " + entity);
        }
        if (entity.getSize() == null) {
            LOGGER.error("replace called with incomplete object {}", entity);
            throw new RepositoryException("replace called with incomplete object " + entity);
        }
        update(entity);
    }

    @Override
    public void update(Property entity) {
        LOGGER.info("update owner with id {}", entity.getId());
        String query = "update property set property_type=?, size=? where id=?";
        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, entity.getType().toString());
            preparedStatement.setDouble(2, entity.getSize());
            preparedStatement.setLong(3, entity.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows < 1) {
                LOGGER.error("Could not edit property with id {}", entity.getId());
                throw new RepositoryException("Could not edit property with id " + entity.getId());
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
    }

    @Override
    public void delete(Property entity) {
        LOGGER.info("delete property with id {}", entity.getId());
        String query = "delete from property where id=?";
        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, entity.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows < 1) {
                LOGGER.error("Could not delete property with id {}", entity.getId());
                throw new RepositoryException("Could not delete property with id" + entity.getId());
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
    }

    @Override
    public Collection<Property> findAll() {
        LOGGER.info("find all properties");
        List<Property> properties = new LinkedList<>();
        String query = "select * from property";
        try (Connection connection = ConnectionManager.getConnection();
                Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                properties.add(resultSetToProperty(resultSet));
            }

            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return properties;
    }

    @Override
    public Property findById(Long id) {
        LOGGER.info("find Property with id {}", id);
        if (id == null) {
            return null;
        }

        Property entity;
        String query = "select * from property where id=?";
        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                entity = resultSetToProperty(resultSet);
            } else {
                LOGGER.error("Could not find property with id: {}", id);
                throw new RepositoryException("Could not find property with id: " + id);
            }
            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return entity;
    }

    private Property resultSetToProperty(ResultSet resultSet) throws SQLException {
        Property property = new Property();
        property.setUuid(UUID.fromString(resultSet.getString("uuid")));
        property.setId(resultSet.getLong("id"));
        property.setSize(resultSet.getDouble("size"));
        property.setType(PropertyType.valueOf(resultSet.getString("property_type")));
        return property;
    }

    @Override
    public Collection<Property> findAllPropertiesBySizeInterval(Double minSize, Double maxSize) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findPropertyListingsById(Long id) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }
}
