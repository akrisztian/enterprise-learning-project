package edu.bbte.idde.akim1784.backend.repository.util;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.akim1784.backend.config.ConfigurationFactory;
import edu.bbte.idde.akim1784.backend.config.JdbcConfiguration;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManager.class);

    public static Connection getConnection() {
        try {
            return DataSourceHolder.DATA_SOURCE.getConnection();
        } catch (SQLException e) {
            LOGGER.error("Could not get connection!");
            throw new RepositoryException("Could not get connection!", e);
        }
    }

    private static class DataSourceHolder {
        static final HikariDataSource DATA_SOURCE = new HikariDataSource();

        static {
            LOGGER.info("Init HikariCP");
            JdbcConfiguration configuration = ConfigurationFactory.getConfiguration().getJdbcConfiguration();
            DATA_SOURCE.setDriverClassName(configuration.getDriverClassName());
            DATA_SOURCE.setJdbcUrl(configuration.getUrl());
            DATA_SOURCE.setMaximumPoolSize(configuration.getPoolSize());
            DATA_SOURCE.setUsername(configuration.getUser());
            DATA_SOURCE.setPassword(configuration.getPassword());
        }
    }
}
