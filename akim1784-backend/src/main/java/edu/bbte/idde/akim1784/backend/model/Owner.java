package edu.bbte.idde.akim1784.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Owner extends BaseEntity {

    @Basic
    private String name;
    @OneToMany(mappedBy = "owner",
            fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            orphanRemoval = true)
    private List<Listing> listings;

    public Owner() {
        super();
    }

    public Owner(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Owner{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
