package edu.bbte.idde.akim1784.backend.repository;

import edu.bbte.idde.akim1784.backend.model.Listing;

import java.util.Collection;

public interface ListingRepository extends Repository<Listing> {
    Collection<Listing> findAllListingsInPriceInterval(Double minPrice, Double maxPrice);

    Collection<Listing> findAllListingsByOwnerName(String name);

    Collection<Listing> findAllListingsWithFieldProperty();
}
