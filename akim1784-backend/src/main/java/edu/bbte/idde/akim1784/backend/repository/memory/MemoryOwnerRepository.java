package edu.bbte.idde.akim1784.backend.repository.memory;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Owner;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemoryOwnerRepository implements OwnerRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemoryOwnerRepository.class);
    private static final Map<Long, Owner> OWNERS = new ConcurrentHashMap<>();
    private static final AtomicLong ATOMIC_ID = new AtomicLong();

    public MemoryOwnerRepository() {
        LOGGER.warn("Mocking owner data");

        create(new Owner("Pelda Bella"));
        create(new Owner("Selymes Andras"));
    }

    @Override
    public Owner create(Owner entity) {
        entity.setId(ATOMIC_ID.incrementAndGet());
        OWNERS.put(entity.getId(), entity);
        LOGGER.info("create owner with id {}", entity.getId());
        return entity;
    }

    @Override
    public void replace(Owner entity) {
        LOGGER.info("replace owner with id {}", entity.getId());
        if (entity.getName() == null) {
            LOGGER.error("replace called with incomplete object {}", entity);
            throw new RepositoryException("Entity is not complete");
        }

        Owner originalOwner = OWNERS.get(entity.getId());
        originalOwner.setName(entity.getName());
    }

    @Override
    public void update(Owner entity) {
        LOGGER.info("update owner with id {}", entity.getId());
        Owner originalOwner = OWNERS.get(entity.getId());
        originalOwner.setName(entity.getName());
    }

    @Override
    public void delete(Owner entity) {
        LOGGER.info("delete owner with id {}", entity.getId());
        OWNERS.remove(entity.getId());
    }

    @Override
    public Collection<Owner> findAll() {
        LOGGER.info("find all owners");
        return OWNERS.values();
    }

    @Override
    public Owner findById(Long id) {
        LOGGER.info("find owner with id {}", id);
        if (OWNERS.containsKey(id)) {
            return OWNERS.get(id);
        } else {
            throw new RepositoryException("No Owner with ID: " + id);
        }
    }

    @Override
    public Collection<Owner> findOwnerByName(String name) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findOwnerListingsById(Long id) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }
}
