package edu.bbte.idde.akim1784.backend.service.impl;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.repository.ListingRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import edu.bbte.idde.akim1784.backend.service.ListingService;
import edu.bbte.idde.akim1784.backend.service.ServiceException;

import java.util.Collection;

public class ListingServiceImpl implements ListingService {

    private final ListingRepository listingRepository;

    public ListingServiceImpl(ListingRepository listingRepository) {
        this.listingRepository = listingRepository;
    }

    @Override
    public Listing createListing(Listing listing) {
        try {
            return listingRepository.create(listing);
        } catch (RepositoryException re) {
            throw new ServiceException("Could not create listing", re);
        }
    }

    @Override
    public void replaceListing(Listing listing) {
        try {
            listingRepository.replace(listing);
        } catch (RepositoryException re) {
            throw new ServiceException("Could not replace listing", re);
        }
    }

    @Override
    public void updateListing(Listing listing) {
        try {
            listingRepository.update(listing);
        } catch (RepositoryException re) {
            throw new ServiceException("Could not update listing", re);
        }
    }

    @Override
    public void deleteListing(Listing listing) {
        try {
            listingRepository.delete(listing);
        } catch (RepositoryException re) {
            throw new ServiceException("Could not delete listing", re);
        }
    }

    @Override
    public Collection<Listing> findAllListings() {
        try {
            return listingRepository.findAll();
        } catch (RepositoryException re) {
            throw new ServiceException("Could not get listings", re);
        }
    }

    @Override
    public Listing findListingById(Long id) {
        try {
            return listingRepository.findById(id);
        } catch (RepositoryException re) {
            throw new ServiceException("Could not find listing with ID: " + id, re);
        }
    }
}
