package edu.bbte.idde.akim1784.backend.repository.jpa;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.PropertyType;
import edu.bbte.idde.akim1784.backend.repository.ListingRepository;

import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.Locale;
import java.util.stream.Collectors;

public class JpaListingRepository extends JpaRepository<Listing> implements ListingRepository {
    public JpaListingRepository() {
        super(Listing.class);
    }

    @Override
    public Collection<Listing> findAllListingsInPriceInterval(Double minPrice, Double maxPrice) {
        logger.info("find all listings by price interval");
        TypedQuery<Listing> typedQuery = entityManager
                .createQuery("select l from Listing l where l.price between :min and :max", Listing.class);
        typedQuery.setParameter("min", minPrice);
        typedQuery.setParameter("max", maxPrice);
        return typedQuery.getResultList();
    }

    @Override
    public Collection<Listing> findAllListingsByOwnerName(String name) {
        logger.info("find all listings by owner name");
        return findAll()
                .stream()
                .filter(listing -> name
                        .toUpperCase(Locale.getDefault())
                        .equals(listing.getOwner().getName().toUpperCase(Locale.getDefault())))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Listing> findAllListingsWithFieldProperty() {
        logger.info("find all listings with property where type = FIELD");
        return findAll()
                .stream()
                .filter(listing -> PropertyType.FIELD.equals(listing.getProperty().getType()))
                .collect(Collectors.toList());
    }
}
