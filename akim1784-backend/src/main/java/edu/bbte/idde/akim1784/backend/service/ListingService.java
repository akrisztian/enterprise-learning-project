package edu.bbte.idde.akim1784.backend.service;

import edu.bbte.idde.akim1784.backend.model.Listing;

import java.util.Collection;

public interface ListingService {

    Listing createListing(Listing listing);

    void replaceListing(Listing listing);

    void updateListing(Listing listing);

    void deleteListing(Listing listing);

    Collection<Listing> findAllListings();

    Listing findListingById(Long id);
}
