package edu.bbte.idde.akim1784.backend.repository;

import java.util.Collection;

public interface Repository<T> {
    T create(T entity);

    void replace(T entity);

    void update(T entity);

    void delete(T entity);

    Collection<T> findAll();
    
    T findById(Long id);
}
