package edu.bbte.idde.akim1784.backend.repository;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Owner;

import java.util.Collection;

public interface OwnerRepository extends Repository<Owner> {
    Collection<Owner> findOwnerByName(String name);

    Collection<Listing> findOwnerListingsById(Long id);
}
