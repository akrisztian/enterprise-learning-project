package edu.bbte.idde.akim1784.backend.model;

public enum Currency {
    RON,
    DOLLAR,
    EURO,
    STERLING;
}
