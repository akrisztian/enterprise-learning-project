package edu.bbte.idde.akim1784.backend.model;

public enum PropertyType {
    FIELD,
    HOUSE,
    APARTMENT
}
