package edu.bbte.idde.akim1784.backend.repository.jpa;

import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Owner;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;

import javax.persistence.TypedQuery;
import java.util.Collection;

public class JpaOwnerRepository extends JpaRepository<Owner> implements OwnerRepository {
    public JpaOwnerRepository() {
        super(Owner.class);
    }

    @Override
    public Collection<Owner> findOwnerByName(String name) {
        logger.info("Find all Owners by Name");
        TypedQuery<Owner> typedQuery = entityManager
                .createQuery("select o from Owner o where o.name like :name", Owner.class);
        typedQuery.setParameter("name", name);
        return typedQuery.getResultList();
    }

    @Override
    public Collection<Listing> findOwnerListingsById(Long id) {
        logger.info("Find listing owner listings by id");
        return findById(id).getListings();
    }
}
