package edu.bbte.idde.akim1784.backend.repository;

import edu.bbte.idde.akim1784.backend.config.ConfigurationFactory;
import edu.bbte.idde.akim1784.backend.config.RepositoryType;
import edu.bbte.idde.akim1784.backend.repository.jdbc.JdbcRepositoryFactory;
import edu.bbte.idde.akim1784.backend.repository.jpa.JpaRepositoryFactory;
import edu.bbte.idde.akim1784.backend.repository.memory.MemoryRepositoryFactory;

public abstract class RepositoryFactory {

    public static RepositoryFactory getInstance() {
        RepositoryType type = ConfigurationFactory.getConfiguration().getRepositoryConfiguration().getType();
        if (type == RepositoryType.JDBC) {
            return JdbcHolder.JDBC_REPOSITORY_FACTORY;
        } else if (type == RepositoryType.JPA) {
            return JpaHolder.JPA_REPOSITORY_FACTORY;
        } else {
            return MemoryHolder.MEMORY_REPOSITORY_FACTORY;
        }
    }

    public abstract ListingRepository getListingRepository();

    public abstract OwnerRepository getOwnerRepository();

    public abstract PropertyRepository getPropertyRepository();

    private static class MemoryHolder {
        static final MemoryRepositoryFactory MEMORY_REPOSITORY_FACTORY = new MemoryRepositoryFactory();
    }

    private static class JdbcHolder {
        static final JdbcRepositoryFactory JDBC_REPOSITORY_FACTORY = new JdbcRepositoryFactory();
    }

    private static class JpaHolder {
        static final JpaRepositoryFactory JPA_REPOSITORY_FACTORY = new JpaRepositoryFactory();
    }
}
