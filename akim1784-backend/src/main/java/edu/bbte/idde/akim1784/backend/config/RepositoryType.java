package edu.bbte.idde.akim1784.backend.config;

public enum RepositoryType {
    MEMORY,
    JDBC,
    JPA,
}
