package edu.bbte.idde.akim1784.backend.service;

import edu.bbte.idde.akim1784.backend.repository.RepositoryFactory;
import edu.bbte.idde.akim1784.backend.service.impl.ListingServiceImpl;

public class ServiceFactory {

    public static final ServiceFactory INSTANCE = new ServiceFactory();

    public static ServiceFactory getInstance() {
        return INSTANCE;
    }

    public ListingService getListingService() {
        return new ListingServiceImpl(RepositoryFactory.getInstance().getListingRepository());
    }

}
