package edu.bbte.idde.akim1784.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigurationFactory {

    public static MainConfiguration getConfiguration() {
        return ConfigHolder.mainConfiguration;
    }

    private static class ConfigHolder {
        private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationFactory.class);
        static MainConfiguration mainConfiguration;

        static {
            String configFileName = getConfigFileName();
            try (InputStream inputStream = ConfigurationFactory.class.getResourceAsStream(configFileName)) {
                mainConfiguration = new ObjectMapper().readValue(inputStream, MainConfiguration.class);
                LOGGER.info("Read config from {}", configFileName);
            } catch (IOException | IllegalArgumentException e) {
                LOGGER.error("Could not load config from {}, using default configuration", configFileName);
                mainConfiguration = new MainConfiguration();
            }
        }

        private static String getConfigFileName() {
            LOGGER.info("Loading properties");
            StringBuilder sb = new StringBuilder();
            sb.append("/application");

            String profile = System.getProperty("profile");
            if (profile == null || profile.isEmpty()) {
                LOGGER.info("Using default profile");
                sb.append('-').append("dev");
            } else {
                LOGGER.info("Detected profile {}", profile);
                sb.append('-').append(profile);
            }
            return sb.append(".json").toString();
        }
    }
}
