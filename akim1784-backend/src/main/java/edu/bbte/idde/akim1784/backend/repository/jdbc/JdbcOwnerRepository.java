package edu.bbte.idde.akim1784.backend.repository.jdbc;

import edu.bbte.idde.akim1784.backend.config.ConfigurationFactory;
import edu.bbte.idde.akim1784.backend.model.Listing;
import edu.bbte.idde.akim1784.backend.model.Owner;
import edu.bbte.idde.akim1784.backend.repository.OwnerRepository;
import edu.bbte.idde.akim1784.backend.repository.RepositoryException;
import edu.bbte.idde.akim1784.backend.repository.util.ConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class JdbcOwnerRepository implements OwnerRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcOwnerRepository.class);

    public JdbcOwnerRepository() {
        if (ConfigurationFactory.getConfiguration().getJdbcConfiguration().getCreateTables()) {
            StringBuilder sb = new StringBuilder();
            sb.append("create table if not exists owner(\n");
            sb.append("id serial primary key not null,\n");
            sb.append("uuid varchar(512) not null, \n");
            sb.append("name varchar(256) );");

            try (Connection connection = ConnectionManager.getConnection();
                    Statement statement = connection.createStatement()) {
                statement.executeUpdate(sb.toString());
            } catch (SQLException e) {
                LOGGER.error("SQL Error");
                throw new RepositoryException("SQL Error", e);
            }
        }
    }

    @Override
    public Owner create(Owner entity) {
        String query = "insert into owner(uuid, name) values (?,?)";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement =
                        connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, entity.getUuid().toString());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            } else {
                generatedKeys.close();
                LOGGER.error("Could not insert entity into owner table");
                throw new RepositoryException("Could not insert entity into owner table");
            }
            generatedKeys.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return entity;
    }

    @Override
    public void replace(Owner entity) {
        if (entity.getName() == null) {
            LOGGER.error("replace called with incomplete object {}", entity);
            throw new RepositoryException("replace called with incomplete object " + entity);
        }
        update(entity);
    }

    @Override
    public void update(Owner entity) {
        LOGGER.info("update owner with id {}", entity.getId());
        String query = "update owner set name=? where id=?";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setLong(2, entity.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows < 1) {
                LOGGER.error("Could not edit owner with id {}", entity.getId());
                throw new RepositoryException("Could not edit owner with id " + entity.getId());
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
    }

    @Override
    public void delete(Owner entity) {
        LOGGER.info("delete owner with id {}", entity.getId());
        String query = "delete from owner where id=?";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, entity.getId());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows < 1) {
                LOGGER.error("Could not delete owner with id {}", entity.getId());
                throw new RepositoryException("Could not delete owner with id" + entity.getId());
            }
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
    }

    @Override
    public Collection<Owner> findAll() {
        LOGGER.info("find all owners");
        List<Owner> owners = new LinkedList<>();
        String query = "select * from owner";

        try (Connection connection = ConnectionManager.getConnection();
                Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                owners.add(resultSetToOwner(resultSet));
            }

            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return owners;
    }

    @Override
    public Owner findById(Long id) {
        LOGGER.info("find Owner with id {}", id);
        if (id == null) {
            return null;
        }

        Owner entity;
        String query = "select * from owner where id=?";

        try (Connection connection = ConnectionManager.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                entity = resultSetToOwner(resultSet);
            } else {
                LOGGER.error("Could not find owner with id: {}", id);
                throw new RepositoryException("Could not find owner with id: " + id);
            }
            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("SQL Error");
            throw new RepositoryException("SQL Error", e);
        }
        return entity;
    }

    private Owner resultSetToOwner(ResultSet resultSet) throws SQLException {
        Owner owner = new Owner();
        owner.setUuid(UUID.fromString(resultSet.getString("uuid")));
        owner.setId(resultSet.getLong("id"));
        owner.setName(resultSet.getString("name"));
        return owner;
    }

    @Override
    public Collection<Owner> findOwnerByName(String name) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }

    @Override
    public Collection<Listing> findOwnerListingsById(Long id) {
        throw new RepositoryException("Method not implemented.", new UnsupportedOperationException());
    }
}
