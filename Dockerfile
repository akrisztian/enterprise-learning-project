FROM antalkrisztian/gradle:6.5-jdk8-alpine as builder
WORKDIR /app
COPY . ./
RUN gradle war --no-daemon

FROM tomcat:8.5-jre8-alpine
RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY --from=builder /app/akim1784-web/build/libs/akim1784-web-1.0.0-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
