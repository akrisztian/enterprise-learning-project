package edu.bbte.idde.akim1784.web.servlet;

import com.github.jknack.handlebars.internal.lang3.StringUtils;
import edu.bbte.idde.akim1784.backend.model.*;
import edu.bbte.idde.akim1784.backend.service.ListingService;
import edu.bbte.idde.akim1784.backend.service.ServiceException;
import edu.bbte.idde.akim1784.backend.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@WebServlet(name = "ListingServlet", urlPatterns = {"/listings/*"})
public class ListingServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListingServlet.class);
    private static final ListingService LISTING_SERVICE = ServiceFactory.getInstance().getListingService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id") == null || req.getParameter("id").equals("")) {
            LOGGER.info("GET /listings");
            PrintWriter writer = resp.getWriter();
            LISTING_SERVICE.findAllListings().stream().forEach(writer::println);
        } else {
            Long id = null;
            try {
                id = Long.parseLong(req.getParameter("id"));
                LOGGER.info("GET /listings/{}", id);
                Listing listing = LISTING_SERVICE.findListingById(id);
                resp.getWriter().println(listing);
            } catch (NumberFormatException ex) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "ID should be a number");
            } catch (ServiceException ex) {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No listing with ID: " + id);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("POST /api/listings");

        if (allRequiredParams(req, resp, Arrays.asList("owner", "type", "size", "price", "currency"))) {
            try {
                Owner owner = new Owner(req.getParameter("owner"));
                Property property = new Property(
                        PropertyType.valueOf(req.getParameter("type")),
                        Double.parseDouble(req.getParameter("size")));

                Listing listing = new Listing();
                listing.setPrice(Double.parseDouble(req.getParameter("price")));
                listing.setDescription(req.getParameter("description"));
                listing.setCurrency(Currency.valueOf(req.getParameter("currency")));
                listing.setDate(LocalDate.now());
                listing.setProperty(property);
                listing.setOwner(owner);

                // check if request was forwarded
                if (req.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI) == null) {
                    resp.getWriter().println(LISTING_SERVICE.createListing(listing));
                } else {
                    LISTING_SERVICE.createListing(listing);
                    resp.sendRedirect(req.getContextPath() + "/");
                }

            } catch (NumberFormatException ex) {
                LOGGER.error("Could not parse double");
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error while parsing double");
            } catch (IllegalArgumentException ex) {
                LOGGER.error("Could not parse enum");
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error while parsing enum");
            } catch (ServiceException ex) {
                LOGGER.error("Could not save listing");
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Could not save listing");
            }
        }
    }

    private boolean allRequiredParams(HttpServletRequest req,
                                      HttpServletResponse resp,
                                      List<String> params) throws IOException {
        for (String param : params) {
            if (StringUtils.isEmpty(req.getParameter(param))) {
                LOGGER.error("Missing parameter " + param);
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
                        param.toUpperCase(Locale.getDefault()) + " is needed");
                return false;
            }
        }

        return true;
    }
}
