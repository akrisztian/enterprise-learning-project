package edu.bbte.idde.akim1784.web.servlet;

import edu.bbte.idde.akim1784.web.util.HandlebarsTemplateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("GET " + req.getRequestURI());
        HandlebarsTemplateFactory.getTemplate("login").apply(false, resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("POST " + req.getRequestURI());
        if (USERNAME.equals(req.getParameter("username"))
                && PASSWORD.equals(req.getParameter("password"))) {
            HttpSession session = req.getSession(true);
            session.setAttribute("loggedIn", true);
            resp.sendRedirect(req.getContextPath() + "/");
        } else {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            HandlebarsTemplateFactory.getTemplate("login").apply(true, resp.getWriter());
        }
    }
}
