package edu.bbte.idde.akim1784.web.servlet;

import edu.bbte.idde.akim1784.backend.model.Currency;
import edu.bbte.idde.akim1784.backend.model.PropertyType;
import edu.bbte.idde.akim1784.backend.service.ListingService;
import edu.bbte.idde.akim1784.backend.service.ServiceFactory;
import edu.bbte.idde.akim1784.web.util.HandlebarsTemplateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet(name = "FrontPageServlet", urlPatterns = {"", "/index"})
public class FrontPageServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontPageServlet.class);
    private static final ListingService LISTING_SERVICE = ServiceFactory.getInstance().getListingService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("GET " + req.getRequestURI());

        Map<String, Object> model = new ConcurrentHashMap<>();
        model.put("listings", LISTING_SERVICE.findAllListings());
        model.put("types", EnumSet.allOf(PropertyType.class));
        model.put("currencies", EnumSet.allOf(Currency.class));

        HandlebarsTemplateFactory.getTemplate("index").apply(model, resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("POST " + req.getRequestURI());
        getServletContext().getRequestDispatcher("/listings").forward(req, resp);
    }
}
