import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Subject } from 'rxjs';

import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Currency } from '../model/currency.enum';
import { ListingDto } from '../model/dto/listing.dto';
import { Listing } from '../model/listing.model';
import { PropertyType } from '../model/property-type.enum';
import { ListingService } from '../services/listing.service';
import { OwnerService } from '../services/owner.service';
import { PropertyService } from '../services/property.service';

@Component({
  selector: 'app-listing-editor',
  templateUrl: './listing-editor.component.html',
  styleUrls: ['./listing-editor.component.scss'],
})
export class ListingEditorComponent implements OnInit {
  listingDto: ListingDto;
  currencies = Currency;
  ownerName = '';
  ownerExists = false;
  searchingOwner = false;
  propertyType: PropertyType;
  propertyExists = false;
  searchingProperty = false;
  listingForm: FormGroup;
  ownerIdChanged = new Subject<number>();
  propertyIdChanged = new Subject<number>();

  constructor(
    private listingService: ListingService,
    private ownerService: OwnerService,
    private propertyService: PropertyService,
    public dialogRef: MatDialogRef<ListingEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public listing: Listing,
  ) {}

  ngOnInit(): void {
    this.listingDto = this.modelToDto(this.listing);
    this.createForm();

    this.ownerIdChanged
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe((id) => {
        this.searchingOwner = true;
        this.ownerService.getOwnerById(id).subscribe(
          (owner) => {
            this.ownerName = owner.name;
            this.ownerExists = true;
            this.listingForm.get('ownerId').setErrors(null);
            setTimeout(() => (this.searchingOwner = false), 500);
          },
          () => {
            this.ownerExists = false;
            this.listingForm
              .get('ownerId')
              .setErrors({ noOwnerWithId: id });
            setTimeout(() => (this.searchingOwner = false), 500);
          },
        );
      });

    this.propertyIdChanged
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe((id) => {
        this.searchingProperty = true;
        this.propertyService.getPropertyById(id).subscribe(
          (property) => {
            this.propertyType = property.type;
            this.propertyExists = true;
            this.listingForm.get('propertyId').setErrors(null);
            setTimeout(() => (this.searchingProperty = false), 1000);
          },
          () => {
            this.propertyExists = false;
            this.listingForm
              .get('propertyId')
              .setErrors({ noPropertyWithId: id });
            setTimeout(() => (this.searchingProperty = false), 1000);
          },
        );
      });

    this.onOwnerIdChange(this.listingDto.ownerId);
    this.onPropertyIdChange(this.listingDto.propertyId);
  }

  createForm(): void {
    this.listingForm = new FormGroup({
      price: new FormControl(
        this.listingDto.price,
        Validators.required.bind(this),
      ),
      currency: new FormControl(
        this.listingDto.currency,
        Validators.required.bind(this),
      ),
      description: new FormControl(this.listingDto.description, [
        Validators.required.bind(this),
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      ownerId: new FormControl(
        this.listingDto.ownerId,
        Validators.required.bind(this),
      ),
      propertyId: new FormControl(
        this.listingDto.propertyId,
        Validators.required.bind(this),
      ),
    });
  }

  isFormControlValid(formControlName: string): boolean {
    return this.listingForm.get(formControlName).valid;
  }

  onOwnerIdChange(id: number): void {
    this.ownerIdChanged.next(id);
  }

  onPropertyIdChange(id: number): void {
    this.propertyIdChanged.next(id);
  }

  modelToDto(listing: Listing): ListingDto {
    return {
      description: listing.description,
      price: listing.price,
      currency: listing.currency,
      ownerId: listing.owner.id,
      propertyId: listing.property.id,
    };
  }

  createProperty(): void {
    this.listingService.createListing(this.listingDto).subscribe(
      (listing) => {
        this.listing = listing;
        this.dialogRef.close();
      },
      (error) => alert(error.error.causes.join('\n')),
    );
  }

  updateProperty(): void {
    this.listingService
      .updateListing(this.listing.id, this.listingDto)
      .subscribe(
        (listing) => {
          this.listing = listing;
          this.dialogRef.close();
        },
        (error) => alert(error.error.causes.join('\n')),
      );
  }

  onSaveClicked(): void {
    if (this.listingForm.valid) {
      this.listingDto = {
        ...this.listingDto,
        ...(this.listingForm.value as ListingDto),
      };
      if (this.listing.id) {
        this.updateProperty();
      } else {
        this.createProperty();
      }
    }
  }
}
