import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ListingEditorComponent } from '../listing-editor/listing-editor.component';
import { Owner } from '../model/owner.model';
import { OwnerEditorComponent } from '../owner-editor/owner-editor.component';
import { ListingService } from '../services/listing.service';
import { OwnerService } from '../services/owner.service';

@Component({
  selector: 'app-owners-dashboard',
  templateUrl: './owners-dashboard.component.html',
  styleUrls: ['../dashboard/dashboard.component.scss'],
})
export class OwnersDashboardComponent implements OnInit {
  owners: Owner[];
  page = 1;
  size = 10;
  totalElements: number;
  cols: number;

  constructor(
    private ownerService: OwnerService,
    private listingService: ListingService,
    private dialogService: MatDialog,
  ) {}

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    if (window.innerWidth <= 992) {
      this.cols = 1;
    } else if (window.innerWidth <= 1200) {
      this.cols = 2;
    } else {
      this.cols = 3;
    }
  }

  ngOnInit(): void {
    if (window.innerWidth <= 992) {
      this.cols = 1;
    } else if (window.innerWidth <= 1200) {
      this.cols = 2;
    } else {
      this.cols = 3;
    }
    this.getOwners();
  }

  getOwners(): void {
    this.ownerService
      .getNOwnersOnPage(this.size, this.page)
      .subscribe((response) => {
        this.totalElements = Number(
          response.headers.get('X-Total-Count'),
        );
        this.owners = response.body;
      });
  }

  onPageChanged(event: { page: number; size: number }): void {
    this.size = event.size;
    this.page = event.page;
    this.getOwners();
  }

  openEditor(owner: Owner): void {
    const ref = this.dialogService.open(OwnerEditorComponent, {
      data: owner,
    });

    ref.afterClosed().subscribe(() => this.getOwners());
  }

  openListingEditor(listingId: number): void {
    this.listingService.getListingById(listingId).subscribe(
      (listing) =>
        this.dialogService.open(ListingEditorComponent, {
          data: listing,
        }),
      (error) => alert(error.error.causes.join('\n')),
    );
  }

  addOwner(): void {
    const owner: Owner = { name: '' };

    const ref = this.dialogService.open(OwnerEditorComponent, {
      data: owner,
    });

    ref.afterClosed().subscribe(() => this.getOwners());
  }

  deleteOwner(id: number): void {
    this.ownerService.deleteOwnerById(id).subscribe(
      () => this.getOwners(),
      (error) => alert(error.error.causes.join('\n')),
    );
  }
}
