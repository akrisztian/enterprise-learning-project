import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PropertyDto } from '../model/dto/property.dto';
import { Property } from '../model/property.model';

@Injectable({
  providedIn: 'root',
})
export class PropertyService {
  constructor(private http: HttpClient) {}

  getNPropertiesOnPage(
    size: number,
    page: number,
  ): Observable<HttpResponse<Property[]>> {
    return this.http.get<Property[]>(
      `api/properties?${page ? `page=${page}&` : ''}${
        size ? `size=${size}` : ''
      }`,
      { observe: 'response' },
    );
  }

  getPropertyById(id: number): Observable<Property> {
    return this.http.get<Property>(`api/properties/${id}`);
  }

  createProperty(propertyDto: PropertyDto): Observable<Property> {
    return this.http.post<Property>('api/properties', propertyDto);
  }

  updateProperty(
    id: number,
    propertyDto: PropertyDto,
  ): Observable<Property> {
    return this.http.patch<Property>(
      `api/properties/${id}`,
      propertyDto,
    );
  }

  deletePropertyById(id: number): Observable<any> {
    return this.http.delete<any>(`api/properties/${id}`);
  }
}
