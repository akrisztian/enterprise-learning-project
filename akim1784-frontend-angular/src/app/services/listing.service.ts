import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListingDto } from '../model/dto/listing.dto';
import { Listing } from '../model/listing.model';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  constructor(private http: HttpClient) {}

  getNListingsOnPage(
    size: number,
    page: number,
  ): Observable<HttpResponse<Listing[]>> {
    return this.http.get<Listing[]>(
      `api/listings?${page ? `page=${page}&` : ''}${
        size ? `size=${size}` : ''
      }`,
      { observe: 'response' },
    );
  }

  getListingById(id: number): Observable<Listing> {
    return this.http.get<Listing>(`api/listings/${id}`);
  }

  createListing(listingDto: ListingDto): Observable<Listing> {
    return this.http.post<Listing>('api/listings', listingDto);
  }

  updateListing(
    id: number,
    listingDto: ListingDto,
  ): Observable<Listing> {
    return this.http.patch<Listing>(`api/listings/${id}`, listingDto);
  }

  deleteListingById(id: number): Observable<any> {
    return this.http.delete<any>(`api/listings/${id}`);
  }
}
