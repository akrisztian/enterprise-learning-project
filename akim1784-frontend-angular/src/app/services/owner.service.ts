import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OwnerDto } from '../model/dto/owner.dto';
import { Owner } from '../model/owner.model';

@Injectable({
  providedIn: 'root',
})
export class OwnerService {
  constructor(private http: HttpClient) {}

  getNOwnersOnPage(
    size: number,
    page: number,
  ): Observable<HttpResponse<Owner[]>> {
    return this.http.get<Owner[]>(
      `api/owners?${page ? `page=${page}&` : ''}${
        size ? `size=${size}` : ''
      }`,
      { observe: 'response' },
    );
  }

  getOwnerById(id: number): Observable<Owner> {
    return this.http.get<Owner>(`api/owners/${id}`);
  }

  createOwner(ownerDto: OwnerDto): Observable<Owner> {
    return this.http.post<Owner>('api/owners', ownerDto);
  }

  updateOwner(id: number, ownerDto: OwnerDto): Observable<Owner> {
    return this.http.patch<Owner>(`api/owners/${id}`, ownerDto);
  }

  deleteOwnerById(id: number): Observable<any> {
    return this.http.delete<any>(`api/owners/${id}`);
  }
}
