import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ListingEditorComponent } from '../listing-editor/listing-editor.component';
import { Currency } from '../model/currency.enum';
import { Listing } from '../model/listing.model';
import { ListingService } from '../services/listing.service';

@Component({
  selector: 'app-listings-dashboard',
  templateUrl: './listings-dashboard.component.html',
  styleUrls: ['../dashboard/dashboard.component.scss'],
})
export class ListingsDashboardComponent implements OnInit {
  listings: Listing[];
  page = 1;
  size = 10;
  totalElements: number;
  cols: number;

  constructor(
    private listingService: ListingService,
    private dialogService: MatDialog,
  ) {}

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    if (window.innerWidth <= 992) {
      this.cols = 1;
    } else if (window.innerWidth <= 1200) {
      this.cols = 2;
    } else {
      this.cols = 3;
    }
  }

  ngOnInit(): void {
    if (window.innerWidth <= 992) {
      this.cols = 1;
    } else if (window.innerWidth <= 1200) {
      this.cols = 2;
    } else {
      this.cols = 3;
    }
    this.getListings();
  }

  getListings(): void {
    this.listingService
      .getNListingsOnPage(this.size, this.page)
      .subscribe((response) => {
        this.totalElements = Number(
          response.headers.get('X-Total-Count'),
        );
        this.listings = response.body;
      });
  }

  onPageChanged(event: { page: number; size: number }): void {
    this.size = event.size;
    this.page = event.page;
    this.getListings();
  }

  openEditor(listing: Listing): void {
    const ref = this.dialogService.open(ListingEditorComponent, {
      data: listing,
    });

    ref.afterClosed().subscribe(() => this.getListings());
  }

  addListing(): void {
    const listing: Listing = {
      price: 100,
      currency: ('RON' as unknown) as Currency,
      description: '',
      owner: { id: 0 },
      property: { id: 0 },
    };

    const ref = this.dialogService.open(ListingEditorComponent, {
      data: listing,
    });

    ref.afterClosed().subscribe(() => this.getListings());
  }

  deleteListing(id: number): void {
    this.listingService.deleteListingById(id).subscribe(
      () => this.getListings(),
      () => alert('Could not delete Listing'),
    );
  }
}
