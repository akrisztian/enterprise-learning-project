import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ListingEditorComponent } from '../listing-editor/listing-editor.component';
import { PropertyType } from '../model/property-type.enum';
import { Property } from '../model/property.model';
import { PropertyEditorComponent } from '../property-editor/property-editor.component';
import { ListingService } from '../services/listing.service';
import { PropertyService } from '../services/property.service';

@Component({
  selector: 'app-properties-dashboard',
  templateUrl: './properties-dashboard.component.html',
  styleUrls: ['../dashboard/dashboard.component.scss'],
})
export class PropertiesDashboardComponent implements OnInit {
  properties: Property[];
  page = 1;
  size = 10;
  totalElements: number;
  cols: number;

  constructor(
    private propertyService: PropertyService,
    private listingService: ListingService,
    private dialogService: MatDialog,
  ) {}

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    if (window.innerWidth <= 992) {
      this.cols = 1;
    } else if (window.innerWidth <= 1200) {
      this.cols = 2;
    } else {
      this.cols = 3;
    }
  }

  ngOnInit(): void {
    if (window.innerWidth <= 992) {
      this.cols = 1;
    } else if (window.innerWidth <= 1200) {
      this.cols = 2;
    } else {
      this.cols = 3;
    }
    this.getProperties();
  }

  getProperties(): void {
    this.propertyService
      .getNPropertiesOnPage(this.size, this.page)
      .subscribe((response) => {
        this.totalElements = Number(
          response.headers.get('X-Total-Count'),
        );
        this.properties = response.body;
      });
  }

  onPageChanged(event: { page: number; size: number }): void {
    this.size = event.size;
    this.page = event.page;
    this.getProperties();
  }

  openListingEditor(listingId: number): void {
    this.listingService.getListingById(listingId).subscribe(
      (listing) =>
        this.dialogService.open(ListingEditorComponent, {
          data: listing,
        }),
      (error) => alert(error.error.causes.join('\n')),
    );
  }

  openEditor(property: Property): void {
    this.dialogService.open(PropertyEditorComponent, {
      data: property,
    });
  }

  addProperty(): void {
    const property: Property = {
      size: 100,
      type: ('FIELD' as unknown) as PropertyType,
    };
    const ref = this.dialogService.open(PropertyEditorComponent, {
      data: property,
    });

    ref.afterClosed().subscribe(() => this.getProperties());
  }

  deleteProperty(id: number): void {
    this.propertyService.deletePropertyById(id).subscribe(
      () => this.getProperties(),
      () => alert('Could not delete Property'),
    );
  }
}
