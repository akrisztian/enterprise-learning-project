import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { OwnerDto } from '../model/dto/owner.dto';
import { Owner } from '../model/owner.model';
import { OwnerService } from '../services/owner.service';

@Component({
  selector: 'app-owner-editor',
  templateUrl: './owner-editor.component.html',
})
export class OwnerEditorComponent {
  ownerControl = new FormControl(this.owner.name, [
    Validators.required.bind(this),
    Validators.minLength(1),
    Validators.maxLength(255),
  ]);

  constructor(
    private ownerService: OwnerService,
    public dialogRef: MatDialogRef<OwnerEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public owner: Owner,
  ) {}

  createOwner(): void {
    this.ownerService.createOwner(this.owner as OwnerDto).subscribe(
      (owner) => {
        this.owner = owner;
        this.dialogRef.close();
      },
      (error) => alert(error.error.causes.join('\n')),
    );
  }

  updateOwner(): void {
    this.ownerService
      .updateOwner(this.owner.id, this.owner as OwnerDto)
      .subscribe(
        (owner) => {
          this.owner = owner;
          this.dialogRef.close();
        },
        (error) => alert(error.error.causes.join('\n')),
      );
  }

  onSaveClicked(): void {
    if (this.ownerControl.valid) {
      this.owner.name = this.ownerControl.value;
      if (this.owner.id) {
        this.updateOwner();
      } else {
        this.createOwner();
      }
    }
  }
}
