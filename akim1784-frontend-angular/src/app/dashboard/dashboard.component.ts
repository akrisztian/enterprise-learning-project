import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  @Input() length;
  @Input() title;
  @Output() pageChanged = new EventEmitter<{
    page: number;
    size: number;
  }>();
  page = 1;
  size = 10;

  constructor() {}

  onPageChanged(event: PageEvent): void {
    this.page = event.pageIndex + 1;
    this.size = event.pageSize;
    this.pageChanged.emit({ page: this.page, size: this.size });
  }
}
