import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingsDashboardComponent } from './listings-dashboard/listings-dashboard.component';
import { OwnersDashboardComponent } from './owners-dashboard/owners-dashboard.component';
import { PropertiesDashboardComponent } from './properties-dashboard/properties-dashboard.component';
import { PropertyEditorComponent } from './property-editor/property-editor.component';

const routes: Routes = [
  {
    path: '',
    component: ListingsDashboardComponent,
  },
  {
    path: 'listings',
    component: ListingsDashboardComponent,
  },
  {
    path: 'owners',
    component: OwnersDashboardComponent,
  },
  {
    path: 'properties',
    component: PropertiesDashboardComponent,
  },
  {
    path: 'test',
    component: PropertyEditorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
