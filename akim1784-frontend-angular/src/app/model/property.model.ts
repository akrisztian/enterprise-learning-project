import { SimpleListing } from './simple-listing.model';
import { SimpleProperty } from './simple-property.model';

export interface Property extends SimpleProperty {
  listings?: SimpleListing[];
}
