import { BaseEntity } from './base-entity.model';
import { Currency } from './currency.enum';

export interface SimpleListing extends BaseEntity {
  description?: string;
  price?: number;
  date?: string;
  currency?: Currency;
}
