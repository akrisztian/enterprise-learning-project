import { SimpleListing } from './simple-listing.model';
import { SimpleOwner } from './simple-owner.model';

export interface Owner extends SimpleOwner {
  listings?: SimpleListing[];
}
