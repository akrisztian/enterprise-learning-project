import { Currency } from '../currency.enum';

export interface ListingDto {
  description?: string;
  price?: number;
  currency?: Currency;
  ownerId?: number;
  propertyId?: number;
}
