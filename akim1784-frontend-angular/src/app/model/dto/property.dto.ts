import { PropertyType } from '../property-type.enum';

export interface PropertyDto {
  type?: PropertyType;
  size?: number;
}
