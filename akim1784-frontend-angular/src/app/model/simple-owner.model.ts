import { BaseEntity } from './base-entity.model';

export interface SimpleOwner extends BaseEntity {
  name?: string;
}
