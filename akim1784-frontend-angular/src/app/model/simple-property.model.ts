import { BaseEntity } from './base-entity.model';
import { PropertyType } from './property-type.enum';

export interface SimpleProperty extends BaseEntity {
  type?: PropertyType;
  size?: number;
}
