import { SimpleListing } from './simple-listing.model';
import { SimpleOwner } from './simple-owner.model';
import { SimpleProperty } from './simple-property.model';

export interface Listing extends SimpleListing {
  owner?: SimpleOwner;
  property?: SimpleProperty;
}
