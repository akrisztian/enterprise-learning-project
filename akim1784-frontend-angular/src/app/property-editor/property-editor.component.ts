import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { PropertyDto } from '../model/dto/property.dto';
import { PropertyType } from '../model/property-type.enum';
import { Property } from '../model/property.model';
import { PropertyService } from '../services/property.service';

@Component({
  selector: 'app-property-editor',
  templateUrl: './property-editor.component.html',
})
export class PropertyEditorComponent {
  types = PropertyType;
  propertyForm = new FormGroup({
    type: new FormControl(
      this.property.type,
      Validators.required.bind(this),
    ),
    size: new FormControl(
      this.property.size,
      Validators.required.bind(this),
    ),
  });

  constructor(
    private propertyService: PropertyService,
    public dialogRef: MatDialogRef<PropertyEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public property: Property,
  ) {}

  isFormControlValid(formControlName: string): boolean {
    return this.propertyForm.get(formControlName).valid;
  }

  createProperty(): void {
    this.propertyService
      .createProperty(this.property as PropertyDto)
      .subscribe(
        (property) => {
          this.property = property;
          this.dialogRef.close();
        },
        (error) => alert(error.error.causes.join('\n')),
      );
  }

  updateProperty(): void {
    this.propertyService
      .updateProperty(this.property.id, this.property)
      .subscribe(
        (property) => {
          this.property = property;
          this.dialogRef.close();
        },
        (error) => alert(error.error.causes.join('\n')),
      );
  }

  onSaveClicked(): void {
    if (this.propertyForm.valid) {
      this.property = {
        ...this.property,
        ...(this.propertyForm.value as Property),
      };
      if (this.property.id) {
        this.updateProperty();
      } else {
        this.createProperty();
      }
    }
  }
}
