FROM antalkrisztian/gradle:6.5-jdk8-alpine as builder
WORKDIR /app
COPY . ./
RUN gradle bootJar --no-daemon

FROM openjdk:8-jre-alpine
COPY --from=builder /app/akim1784-spring-backend/build/libs/akim1784-spring-backend-1.0.0-SNAPSHOT.jar \
 /app.jar
CMD ["java", "-jar", "app.jar"]