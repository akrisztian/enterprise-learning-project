package edu.bbte.idde.akim1784.desktop;

import edu.bbte.idde.akim1784.desktop.model.ListingContainer;
import edu.bbte.idde.akim1784.desktop.view.ListingFrame;
import org.slf4j.LoggerFactory;

public class Main {
    public static void main(String[] argv) {
        LoggerFactory.getLogger(Main.class).info("starting desktop app");
        ListingContainer listingContainer = new ListingContainer();
        new ListingFrame(listingContainer);
    }
}
