package edu.bbte.idde.akim1784.desktop.model;

import edu.bbte.idde.akim1784.backend.model.Listing;

import javax.swing.table.AbstractTableModel;

public class ListingTableModel extends AbstractTableModel {

    private final ListingContainer listingContainer;
    private final String[] columnNames;

    public ListingTableModel(ListingContainer listingContainer) {
        super();
        this.listingContainer = listingContainer;
        columnNames = new String[] {
                "Owner", "Type", "Size", "Price", "Description"
        };
    }

    @Override
    public int getRowCount() {
        return listingContainer.getListings().size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Listing listing = listingContainer.getListings().get(rowIndex);
        switch (columnIndex) {
            case 0: return listing.getOwner().getName();
            case 1: return listing.getProperty().getType();
            case 2: return listing.getProperty().getSize();
            case 3: return listing.getPrice();
            case 4: return listing.getDescription();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }
}
