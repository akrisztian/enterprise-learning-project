package edu.bbte.idde.akim1784.desktop.view;

import edu.bbte.idde.akim1784.backend.service.ServiceException;
import edu.bbte.idde.akim1784.backend.service.ServiceFactory;
import edu.bbte.idde.akim1784.desktop.model.ListingContainer;
import edu.bbte.idde.akim1784.desktop.model.ListingTableModel;

import javax.swing.*;
import java.awt.*;

public class ListingFrame extends JFrame {

    public ListingFrame(ListingContainer listingContainer) {
        super();
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());

        JButton connectButton = new JButton("List Properties");

        ListingTableModel listingTableModel = new ListingTableModel(listingContainer);
        JTable listingTable = new JTable(listingTableModel);
        JScrollPane tableScrollPane = new JScrollPane(listingTable);
        listingTable.setFillsViewportHeight(true);

        contentPane.add(connectButton, BorderLayout.NORTH);
        contentPane.add(tableScrollPane, BorderLayout.CENTER);

        connectButton.addActionListener(event -> {
            try {
                listingContainer.setListings(
                        ServiceFactory.getInstance().getListingService().findAllListings()
                );
                listingTableModel.fireTableDataChanged();
            } catch (ServiceException ex) {
                showFailPopup("Could not load listings!");
            }
        });

        setContentPane(contentPane);

        setBounds(10, 10, 800, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Real Estate Table");
        setVisible(true);
    }

    public void showFailPopup(String message) {
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
