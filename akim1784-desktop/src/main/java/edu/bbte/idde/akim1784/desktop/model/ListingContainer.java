package edu.bbte.idde.akim1784.desktop.model;

import edu.bbte.idde.akim1784.backend.model.Listing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ListingContainer {

    private final List<Listing> listings;

    public ListingContainer() {
        listings = new ArrayList<>();
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(Collection<Listing> listings) {
        this.listings.clear();
        this.listings.addAll(listings);
    }
}
